import { TestBed } from '@angular/core/testing';

import { CaracteristicasHomeService } from './caracteristicas-home.service';

describe('CaracteristicasHomeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CaracteristicasHomeService = TestBed.get(CaracteristicasHomeService);
    expect(service).toBeTruthy();
  });
});
