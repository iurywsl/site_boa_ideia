import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescComercialComponent } from './desc-comercial.component';

describe('DescComercialComponent', () => {
  let component: DescComercialComponent;
  let fixture: ComponentFixture<DescComercialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescComercialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescComercialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
