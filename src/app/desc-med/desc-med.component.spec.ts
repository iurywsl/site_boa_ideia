import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescMedComponent } from './desc-med.component';

describe('DescMedComponent', () => {
  let component: DescMedComponent;
  let fixture: ComponentFixture<DescMedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescMedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescMedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
