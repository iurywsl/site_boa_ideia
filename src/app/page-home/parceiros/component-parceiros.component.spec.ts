import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentParceirosComponent } from './component-parceiros.component';

describe('ComponentParceirosComponent', () => {
  let component: ComponentParceirosComponent;
  let fixture: ComponentFixture<ComponentParceirosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentParceirosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentParceirosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
