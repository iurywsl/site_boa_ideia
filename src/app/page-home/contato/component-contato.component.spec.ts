import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentContatoComponent } from './component-contato.component';

describe('ComponentContatoComponent', () => {
  let component: ComponentContatoComponent;
  let fixture: ComponentFixture<ComponentContatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentContatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentContatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
