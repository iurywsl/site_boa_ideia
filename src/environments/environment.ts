// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC8FnX633g601VFCiz2tVwpdqYfbE__FcE",
    authDomain: "boaideianetinformatica.firebaseapp.com",
    databaseURL: "https://boaideianetinformatica.firebaseio.com",
    projectId: "boaideianetinformatica",
    storageBucket: "boaideianetinformatica.appspot.com",
    messagingSenderId: "980360100677",
    appId: "1:980360100677:web:a642d6e20da6ab2f"
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
