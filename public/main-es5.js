(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-transparent bg-danger navbar-color-on-scroll fixed-top navbar-expand-lg\" color-on-scroll=\"5\"\r\n  id=\"sectionsNav\">\r\n  <div class=\"container\">\r\n    <div class=\"navbar-translate\">\r\n      <a class=\"navbar-brand\">\r\n     \r\n       <img  alt=\"angular\" class=\"d-inline-block align-middle\"  src=\"../assets/img/logo_bi_final.png\">\r\n       <strong class=\"margin-custom-logo\">Boa Ideia</strong>\r\n       <!--  <strong>{{home1}}<br>{{home2}}</strong>-->\r\n    <!--   <img  alt=\"angular\" class=\"d-inline-block align-top \"  src=\"../assets/img/logo_boaideia.jpeg\">-->\r\n      </a>\r\n      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" aria-expanded=\"false\"\r\n        aria-label=\"Toggle navigation\">\r\n        <span class=\"sr-only\">Toggle navigation</span>\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n    </div>\r\n    <div class=\"collapse navbar-collapse\">\r\n      <ul class=\"navbar-nav ml-auto\">\r\n        <li class=\"nav-item\">\r\n          <a class=\"nav-link\" href=\"/home\">\r\n            <i class=\"material-icons\">home</i> Inicial\r\n          </a>\r\n        </li>\r\n        <li class=\"dropdown nav-item\">\r\n          <a href=\"#\" class=\"dropdown-toggle nav-link\" data-toggle=\"dropdown\">\r\n            <i class=\"material-icons\">apps</i> Navegação\r\n          </a>\r\n          <div class=\"dropdown-menu dropdown-with-icons\">\r\n\r\n                      <a routerLink=\"./desc_med\" class=\"dropdown-item\">\r\n              <i class=\"material-icons\">business</i> Sistema Hospitalar e<br> Clínicas\r\n            </a>\r\n\r\n            <a routerLink=\"./desc_comercial\" class=\"dropdown-item\">\r\n\r\n              <i class=\"material-icons\">store</i> Sistema Comercial\r\n            </a>\r\n            <a routerLink=\"./parceiros\" class=\"dropdown-item\">\r\n              <i class=\"material-icons\">people</i> Parceiros\r\n            </a>\r\n            <a href=\"#contato\" class=\"dropdown-item\">\r\n              <i class=\"material-icons\">local_phone</i> Contato\r\n            </a>\r\n          </div>\r\n        </li>\r\n        <li class=\"dropdown nav-item\">\r\n          <a href=\"#\" class=\"dropdown-toggle nav-link\" data-toggle=\"dropdown\">\r\n            <i class=\"material-icons\">cloud_download</i> Downloads\r\n          </a>\r\n          <div class=\"dropdown-menu dropdown-with-icons\">\r\n\r\n            <a class=\"dropdown-item\" href=\"./assets/downloads/teamviewer.rar\" download=\"teamviewer.rar\">\r\n              <i class=\"material-icons\">cloud_download</i> Team Viewer\r\n            </a>\r\n            <a class=\"dropdown-item\" href=\"./assets/downloads/anydesk.rar\" download=\"anydesk.rar\">\r\n              <i class=\"material-icons\">cloud_download</i> Any Desk\r\n            </a>\r\n            <a class=\"dropdown-item\" href=\"./assets/downloads/winrar32.rar\" download=\"winrar32.rar\">\r\n              <i class=\"material-icons\">cloud_download</i> Winrar 32\r\n            </a>\r\n            <a class=\"dropdown-item\" href=\"./assets/downloads/winrar64.rar\" download=\"winrar64.rar\">\r\n              <i class=\"material-icons\">cloud_download</i> Winrar 64\r\n            </a>\r\n          </div>\r\n        </li>\r\n        <li class=\"nav-item\">\r\n          <a class=\"nav-link\" href=\"/tutoriais\">\r\n            <i class=\"material-icons\">vpn_key</i> Área do Cliente\r\n          </a>\r\n        </li>\r\n        <li *ngIf=\"isLoggin\" class=\"nav-item\">\r\n          <a class=\"nav-link\" href=\"/login\" (click)=\"this.loginService.logout()\">\r\n            <i class=\"material-icons\">reply</i> Sair\r\n          </a>\r\n        </li>\r\n        <li class=\"nav-item\">\r\n          <a class=\"nav-link\"\r\n            href=\"https://api.whatsapp.com/send?phone=5538984069498&text=Oi.%20Gostaria%20de%20um%20atendimento.\"\r\n            rel=\"tooltip\" title=\"\" data-placement=\"bottom\" target=\"_blank\" data-original-title=\"Entre em contato agora\">\r\n            <i class=\"fa fa-whatsapp\"></i>\r\n          </a>\r\n        </li>\r\n        <!--<i class=\"fa fa-facebook-square\"></i>-->\r\n        <li class=\"nav-item\">\r\n          <a class=\"nav-link\" href=\"https://www.instagram.com/boaideianet/\" rel=\"tooltip\" title=\"\"\r\n            data-placement=\"bottom\" target=\"_blank\" data-original-title=\"Veja nosso Instagram\">\r\n            <i class=\"fa fa-instagram\"></i>\r\n          </a>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n</nav>\r\n\r\n\r\n<router-outlet ></router-outlet>\r\n<app-footer></app-footer>\r\n<!--<router-outlet></router-outlet>-->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/desc-comercial/desc-comercial.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/desc-comercial/desc-comercial.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"section page-header header-filter\" id=\"inicial\" data-parallax=\"true\"\r\n  style=\"background-image: url('../assets/img/comercial2.jpg')\">\r\n  <div class=\"container text-center\">\r\n\r\n    <h1 class=\"title\">Software Comercial</h1>\r\n    <h4>_</h4>\r\n    <br>\r\n\r\n\r\n  </div>\r\n</div>\r\n\r\n<div class=\"main main-raised\">\r\n\r\n\r\n  <!-- Init Freature 1 -->\r\n  <div class=\"container\">\r\n    <div class=\"section text-center\" id=\"features\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-8 ml-auto mr-auto\">\r\n          <h2 class=\"title\">Software Comercial</h2>\r\n          <h5 class=\"description\">_</h5>\r\n        </div>\r\n      </div>\r\n      <div class=\"features\">\r\n        <div class=\"row\">\r\n          <div class=\"col-md-4\">\r\n          </div>\r\n          <div class=\"col-md-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-info\">\r\n                <i class=\"material-icons\">business</i>\r\n              </div>\r\n              <h4 class=\"info-title\">Comercial</h4>\r\n              <p>_</p>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-md-4\">\r\n          </div>\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <!-- Init Freature 2 -->\r\n\r\n    <div class=\"features-5 text-center\" style=\"background-image: url('./assets/img/bg9.jpg')\">\r\n      <div class=\"col-md-8 ml-auto mr-auto text-center\">\r\n        <h2 class=\"title\">Modulos do Software</h2>\r\n      </div>\r\n      <div class=\"container\">\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-danger text-center\">\r\n                <i class=\"material-icons\">code</i>\r\n              </div>\r\n              <h4 class=\"info-title text-center\">Area Administrativa</h4>\r\n              <p>.</p>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-danger\">\r\n                <i class=\"material-icons\">format_paint</i>\r\n              </div>\r\n              <h4 class=\"info-title\">NF/ECF/NFCe</h4>\r\n              <p>.</p>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-danger\">\r\n                <i class=\"material-icons\">dashboard</i>\r\n              </div>\r\n              <h4 class=\"info-title\">Controle de Estoque</h4>\r\n              <p>.</p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-danger\">\r\n                <i class=\"material-icons\">view_carousel</i>\r\n              </div>\r\n              <h4 class=\"info-title\">Relatorios</h4>\r\n              <p>.</p>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-danger\">\r\n                <i class=\"material-icons\">access_time</i>\r\n              </div>\r\n              <h4 class=\"info-title\">SINTEGRA / SPED </h4>\r\n              <p>.</p>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-danger\">\r\n                <i class=\"material-icons\">attach_money</i>\r\n              </div>\r\n              <h4 class=\"info-title\">Filtros</h4>\r\n              <p>.</p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-danger\">\r\n                <i class=\"material-icons\">view_carousel</i>\r\n              </div>\r\n              <h4 class=\"info-title\">Movimento de Produto</h4>\r\n              <p>.</p>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-danger\">\r\n                <i class=\"material-icons\">access_time</i>\r\n              </div>\r\n              <h4 class=\"info-title\">Estoque</h4>\r\n              <p>.</p>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-danger\">\r\n                <i class=\"material-icons\">attach_money</i>\r\n              </div>\r\n              <h4 class=\"info-title\">Usuario, Senha e Permissoes</h4>\r\n              <p>.</p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <br>\r\n\r\n\r\n  </div>\r\n\r\n  <!-- Init Section Contato \r\n    <app-component-contato></app-component-contato>-->\r\n  <!-- End Section Contato -->\r\n\r\n\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/desc-med/desc-med.component.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/desc-med/desc-med.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"section page-header header-filter\" id=\"inicial\" data-parallax=\"true\"\r\n  style=\"background-image: url('../assets/img/imagens_tiago/4.jpg')\">\r\n  <div class=\"container text-center\">\r\n\r\n    <h1 class=\"title\">Software Medico</h1>\r\n    <h4>_</h4>\r\n    <br>\r\n\r\n\r\n  </div>\r\n</div>\r\n\r\n\r\n\r\n<div class=\"main main-raised\">\r\n  <!-- Init Freature 1 -->\r\n  <div class=\"container\">\r\n    <div class=\"section text-center\" id=\"features\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-8 ml-auto mr-auto\">\r\n          <h2 class=\"title\">Software Medico</h2>\r\n          <h5 class=\"description\">.</h5>\r\n        </div>\r\n      </div>\r\n      <div class=\"features\">\r\n        <div class=\"row\">\r\n          <div class=\"col-md-6\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-success\">\r\n                <i class=\"material-icons\">opacity</i>\r\n              </div>\r\n              <h4 class=\"info-title\">Clinico</h4>\r\n              <p>.</p>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-md-6\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-danger\">\r\n                <i class=\"material-icons\">add_comment</i>\r\n              </div>\r\n              <h4 class=\"info-title\">Hospitalar</h4>\r\n              <p>.</p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n\r\n    <div class=\"col-md-8 ml-auto mr-auto text-center\">\r\n\r\n      <div style=\"text-align: justify;\">Os sistemas s&atilde;o direcionados para a demanda de cada\r\n        institui&ccedil;&atilde;o de sa&uacute;de, de modo personalizado, permitindo a otimiza&ccedil;&atilde;o de\r\n        recursos e proporcionando a tomada de decis&otilde;es, gerenciando informa&ccedil;&otilde;es cl&iacute;nicas,\r\n        assistenciais, administrativas, financeiras e estrat&eacute;gicas, proporcionando uma gest&atilde;o mais\r\n        eficiente e melhor atendimento para os pacientes.</div>\r\n      <div style=\"text-align: justify;\">Nosso objetivo &eacute; levar, aos nossos clientes, nossa expertise de quase\r\n        30 anos de mercado. Com a implanta&ccedil;&atilde;o de metodologias e ferramentas de gest&atilde;o para obter\r\n        a maximiza&ccedil;&atilde;o de resultados.</div>\r\n      <div>&nbsp;</div>\r\n    </div>\r\n\r\n\r\n    <!-- Init Feature Caracteristica -->\r\n    <div class=\"section text-center\" id=\"carousel\">\r\n\r\n      <div class=\"features-5 text-center\" style=\"background-image: url('./assets/img/bg9.jpg')\">\r\n        <div class=\"col-md-8 ml-auto mr-auto text-center\">\r\n          <h2 class=\"title\">Características</h2>\r\n        </div>\r\n        <div class=\"container\">\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-4\">\r\n              <div class=\"info\">\r\n                <div class=\"icon icon-danger text-center\">\r\n                  <i class=\"material-icons\">contact_phone</i>\r\n                </div>\r\n                <h4 class=\"info-title text-center\">Atendimento</h4>\r\n                <p>Agendamento Consultas e Exames\r\n                  <br>\r\n                  Telemarketing\r\n                  <br>\r\n                  Confirmação de Atendimento via SMS\r\n                  <br>\r\n                  Classificação de Atendimento via SMS\r\n                  <br>\r\n                  Internação\r\n                  <br>\r\n                  Painel de Chamada por Voz\r\n                  <br>\r\n                  Visualizalçao da Agenda via Aplicativo\r\n                  <br>\r\n                  Gestão de Procedimentos Seriados\r\n                  <br>\r\n                  Múltiplos Locais de Atendimento\r\n                  <br>\r\n                  Estatísticas de Atendimento\r\n\r\n\r\n\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-sm-4\">\r\n              <div class=\"info\">\r\n                <div class=\"icon icon-danger\">\r\n                  <!--<i style=\"color:rgb(255,255,255,0);\">X.</i> UTILIZADA PARA DEIXAR UM ESPAÇO ENTRE A img E O TEXTO, o 0 do 255,255,255,0 \r\n            FAZ FICAR TRANSPARENTE-->\r\n                  <!--<img src=\"../assets/img/logo_bi_final.png\" style=\"width:25px;height:25px\"/><i style=\"color:rgb(255,255,255,0);\">X.</i>   Sistema de Clinica-->\r\n\r\n                  <i class=\"material-icons\">pregnant_woman</i>\r\n                  <!--<img src=\"../../../assets/img/assistencial.png\" style=\"width:200px;height:200px\"/>-->\r\n                </div>\r\n                <h4 class=\"info-title\">Assistencial</h4>\r\n                <p>Consultório Médico\r\n                  <br>Prontuário Eletrônico\r\n                  <br>Evolução Médica\r\n                  <br>Prescrição Eletrônica\r\n                  <br>Centro Cicúrgico\r\n                  <br>Internação\r\n                  <br>Solicitação, Acompanhamento<br> e Visualização de Exames\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-sm-4\">\r\n              <div class=\"info\">\r\n                <div class=\"icon icon-danger\">\r\n                  <i class=\"material-icons\">local_atm</i>\r\n                </div>\r\n                <h4 class=\"info-title\">Faturamento</h4>\r\n                <p>\r\n                  Convênio e Particulares\r\n                  <br>\r\n                  Arquivo Eletrônico TISS\r\n                  <br>\r\n                  Faturamento Ambulatorial\r\n                  <br>\r\n                  SUS\r\n                  <br>\r\n                  Gestão de Glosas\r\n                  <br>\r\n                  Controle de Repasse Médico\r\n                  <br>\r\n                  Gestão de Recibos\r\n                  <br>\r\n                  Nota Fiscal de Serviço\r\n                </p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-4\">\r\n              <div class=\"info\">\r\n                <div class=\"icon icon-danger\">\r\n                  <i class=\"material-icons\">note_add</i>\r\n                </div>\r\n                <h4 class=\"info-title\">Suprimentos</h4>\r\n                <p>\r\n                  Gestão e Controle de Estoque\r\n                  <br>\r\n                  Média de Consumo\r\n                  <br>\r\n                  Curva ABC\r\n                  <br>\r\n                  Estoque Mínimo e Máximo\r\n                  <br>\r\n                  Controle por lote e Validade\r\n                  <br>\r\n                  Autorização de Dispensação via Biometria\r\n                  <br>\r\n                  Dispensação via Prescrição Eletrônica\r\n                  <br>\r\n                  Inventário\r\n                  <br>\r\n                  Gestão de Compras\r\n                  <br>\r\n                  Cotação por Email\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-sm-4\">\r\n              <div class=\"info\">\r\n                <div class=\"icon icon-danger\">\r\n                  <i class=\"material-icons\">description</i>\r\n                </div>\r\n                <h4 class=\"info-title\">Financeiro</h4>\r\n                <p>\r\n                  Contas a Pagar\r\n                  Contas a Receber\r\n                  Controle Bancário\r\n                  Fluxo Bancário\r\n                  Fluxo de Caixa\r\n                  Realiza Financeiro - DRE\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-sm-4\">\r\n              <div class=\"info\">\r\n                <div class=\"icon icon-danger\">\r\n                  <i class=\"material-icons\">loyalty</i>\r\n                </div>\r\n                <h4 class=\"info-title\">Exames/Laboratório</h4>\r\n                <p>\r\n                  Acompanhamento Exames\r\n                  Registro de Resultados e Laudos\r\n                  Controle de Entrega\r\n                  Estatisticas de Produção\r\n                  Assinatura Eletrônica de Laudos\r\n                  Painel de Exames Solicitados\r\n                </p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-4\">\r\n            </div>\r\n            <div class=\"col-sm-4\">\r\n              <div class=\"info\">\r\n                <div class=\"icon icon-danger\">\r\n                  <i class=\"material-icons\">credit_card</i>\r\n                </div>\r\n                <h4 class=\"info-title\">Cartão Fidelidade</h4>\r\n                <p>\r\n                  Gestão de Contrato\r\n                  Inadimplência\r\n                  Emissão de Boleto\r\n                  Emissão de Autorização de Atendimento\r\n                  Análise de Viabilidade Contratual\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-sm-4\">\r\n            </div>\r\n          </div>\r\n\r\n\r\n          <!--\r\n        <div class=\"rowUai\">\r\n            <div class=\"columnUaiTwo\">\r\n              <div class=\"info\">\r\n                <div class=\"icon icon-danger\">\r\n                  <i class=\"material-icons\">note_add</i>\r\n                </div>\r\n                <h4 class=\"info-title\">Suprimentos</h4>\r\n                <p>\r\n                  Gestão e Controle de Estoque\r\n                  <br>\r\n                  Média de Consumo\r\n                  <br>\r\n                  Curva ABC\r\n                  <br>\r\n                  Estoque Mínimo e Máximo\r\n                  <br>\r\n                  Controle por lote e Validade\r\n                  <br>\r\n                  Autorização de Dispensação via Biometria\r\n                  <br>\r\n                  Dispensação via Prescrição Eletrônica\r\n                  <br>\r\n                  Inventário\r\n                  <br>\r\n                  Gestão de Compras\r\n                  <br>\r\n                  Cotação por Email\r\n                </p>\r\n              </div>\r\n            </div>\r\n            <div class=\"columnUaiTwo\">\r\n              <div class=\"info\">\r\n                <div class=\"icon icon-danger\">\r\n                  <i class=\"material-icons\">description</i>\r\n                </div>\r\n                <h4 class=\"info-title\">Financeiro</h4>\r\n                <p>\r\n                  Contas a Pagar\r\n                  Contas a Receber\r\n                  Controle Bancário\r\n                  Fluxo Bancário\r\n                  Fluxo de Caixa\r\n                  Realiza Financeiro - DRE\r\n                </p>\r\n              </div>\r\n            </div>            \r\n          </div>\r\n-->\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!-- End Feature Caracteristica -->\r\n\r\n\r\n\r\n    <!-- Init Freature 2 -->\r\n    <div *ngIf=\"showDiv\" class=\"features-5 text-center\" style=\"background-image: url('./assets/img/bg9.jpg')\">\r\n      <div class=\"col-md-8 ml-auto mr-auto text-center\">\r\n        <h2 class=\"title\">Modulos do Software</h2>\r\n      </div>\r\n      <div class=\"container\">\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-danger text-center\">\r\n                <i class=\"material-icons\">code</i>\r\n              </div>\r\n              <h4 class=\"info-title text-center\">Modulo_Nome</h4>\r\n              <p>.</p>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-danger\">\r\n                <i class=\"material-icons\">format_paint</i>\r\n              </div>\r\n              <h4 class=\"info-title\">Modulo Agenda</h4>\r\n              <p>.</p>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-danger\">\r\n                <i class=\"material-icons\">dashboard</i>\r\n              </div>\r\n              <h4 class=\"info-title\">Modulo Farmacia</h4>\r\n              <p>.</p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-danger\">\r\n                <i class=\"material-icons\">view_carousel</i>\r\n              </div>\r\n              <h4 class=\"info-title\">Modulo Prescrição Medica</h4>\r\n              <p>.</p>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-danger\">\r\n                <i class=\"material-icons\">access_time</i>\r\n              </div>\r\n              <h4 class=\"info-title\">Modulo Faturamento</h4>\r\n              <p>.</p>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-danger\">\r\n                <i class=\"material-icons\">attach_money</i>\r\n              </div>\r\n              <h4 class=\"info-title\">Modulo Administrativo</h4>\r\n              <p>.</p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-danger\">\r\n                <i class=\"material-icons\">view_carousel</i>\r\n              </div>\r\n              <h4 class=\"info-title\">Modulo Produto</h4>\r\n              <p>.</p>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-danger\">\r\n                <i class=\"material-icons\">access_time</i>\r\n              </div>\r\n              <h4 class=\"info-title\">Modulo Exames</h4>\r\n              <p>.</p>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-sm-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-danger\">\r\n                <i class=\"material-icons\">attach_money</i>\r\n              </div>\r\n              <h4 class=\"info-title\">Modulo Financeiro</h4>\r\n              <p>.</p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <br>\r\n    <br>\r\n    <br>\r\n    <br>\r\n\r\n  </div>\r\n  <!-- Init Section Contato \r\n    <app-component-contato></app-component-contato>-->\r\n  <!-- End Section Contato -->\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/footer/footer.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/footer/footer.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer class=\"footer footer-default margin-bot\">\n    <div class=\"container\">\n  \n      <div class=\"copyright float-right min-text\">\n        \n        <i class=\"material-icons\">apps</i> \n        Desenvolvido by <a href=\"https://www.linkedin.com/in/gerson-crisostomo-62057865/\">webDev</a>\n                Todos os Direitos Reservados | Copyright ® \n      </div>\n  \n    </div>\n  </footer>\n\n  <!--\n    \n        <img _ngcontent-vpq-c14=\"\" alt=\"angular\" class=\"docs-footer-angular-logo\" src=\"../assets/img/logo_bi_final.png\">\n        Desenvolvido by GersonDev <br>\n        Todos os Direitos Reservados | Copyright ® \n  -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/page-formvideos/list/list.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/page-formvideos/list/list.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"page-header header-filter\" data-parallax=\"true\"\r\n    style=\"background-image: url('../assets/img/clinica1.jpg');height: 200px\">\r\n    <div class=\"container\">\r\n\r\n    </div>\r\n</div>\r\n\r\n<div class=\"main main-raised\">\r\n    <div class=\"container\">\r\n\r\n        <br>\r\n        <br>\r\n\r\n        <div class=\"card\">\r\n            <div class=\"card-header card-header-danger\">\r\n                <h4 class=\"card-title\">Lista de Videos</h4>\r\n                <p class=\"card-category\">Boa Ideia</p>\r\n            </div>\r\n            <div class=\"card-content table-responsive table-full-width\">\r\n                <br>\r\n                <div class=\"container\">\r\n                    <div class=\"row\">\r\n                        <div class=\"container\">\r\n                            <div class=\"btn-group\">\r\n                                <button type=\"button\" class=\"btn btn-danger dropdown-toggle\" data-toggle=\"dropdown\"\r\n                                    aria-haspopup=\"true\" aria-expanded=\"false\">\r\n                                    Categoria\r\n                                </button>\r\n                                <div class=\"dropdown-menu\">\r\n                                    <a *ngFor=\"let cat of listcategoria | async\" class=\"dropdown-item\"\r\n                                        (click)=\"alterarCategoria(cat.nome)\">{{cat.nome}}</a>\r\n                                    <a class=\"dropdown-item\" (click)=\"alterarCategoria('')\">Todas</a>\r\n                                    <!--<a class=\"dropdown-item\">Todos</a>-->\r\n                                </div>\r\n                            </div>\r\n                            <button style=\"float: right;\" type=\"button\" data-toggle=\"modal\" data-target=\"#modalnovo\"\r\n                                class=\"btn btn-success\">Novo Video</button>\r\n                        </div>\r\n                    </div>\r\n                    <br>\r\n                </div>\r\n\r\n                <!-- TABELA DE VIDEOS -->\r\n\r\n                <table class=\"table table-striped\">\r\n                    <thead class=\"text-danger\">\r\n                        <th class=\"text-center\"></th>\r\n                        <th class=\"text-center\">Descrição</th>\r\n                        <th class=\"text-center\">Categoria</th>\r\n                        <th class=\"text-center\">Empresa</th>\r\n                        <th class=\"text-center\">Link</th>\r\n                        <th></th>\r\n                    </thead>\r\n                    <tbody>\r\n                        <tr *ngFor=\"let list of listvideos | async\">\r\n\r\n                            <td class=\"td-actions text-right\">\r\n                                <button type=\"button\" rel=\"tooltip\" class=\"btn btn-success btn-sm\" data-toggle=\"modal\"\r\n                                    data-target=\"#modaleditar\"\r\n                                    (click)=\"preencherForm(list.id,list.descricao,list.link,list.empresa,list.categoria)\">\r\n                                    <i class=\"material-icons\">edit</i>\r\n                                </button>\r\n                                ||\r\n                                <button type=\"button\" rel=\"tooltip\" class=\"btn btn-danger btn-sm\"\r\n                                    (click)=\"deleteVideo(list.id)\">\r\n                                    <i class=\"material-icons\">close</i>\r\n                                </button>\r\n                            </td>\r\n                            <td class=\"text-center\" style=\"vertical-align: middle\">{{list.descricao}}</td>\r\n                            <td class=\"text-center\" style=\"vertical-align: middle\">{{list.categoria}}</td>\r\n                            <td class=\"text-center\" style=\"vertical-align: middle\">{{list.empresa}}</td>\r\n                            <td class=\"text-center\" style=\"vertical-align: middle\">{{list.link}}</td>\r\n\r\n                        </tr>\r\n\r\n\r\n                    </tbody>\r\n                </table>\r\n\r\n                <!-- END TABELA DE VIDEOS -->\r\n\r\n                <br><br>\r\n            </div>\r\n        </div>\r\n\r\n        <br><br>\r\n\r\n        <div class=\"card\">\r\n            <div class=\"card-header card-header-danger\">\r\n                <h4 class=\"card-title\">Lista de Imagens</h4>\r\n                <p class=\"card-category\">Boa Ideia</p>\r\n            </div>\r\n            <div class=\"card-content table-responsive table-full-width\">\r\n                <br>\r\n                <div class=\"container\">\r\n                    <div class=\"row\">\r\n                        <div class=\"container\">\r\n                            <div class=\"btn-group\">\r\n                                <button type=\"button\" class=\"btn btn-danger \" aria-haspopup=\"true\"\r\n                                    aria-expanded=\"false\">\r\n                                    Imagens\r\n                                </button>\r\n\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                    <br>\r\n                </div>\r\n\r\n                <!-- TABELA DE IMAGENS -->\r\n\r\n                <table class=\"table table-striped\">\r\n                    <thead class=\"text-danger\">\r\n\r\n                        <th class=\"text-center\"></th>\r\n                        <th class=\"text-center\">Descrição</th>\r\n                        <th class=\"text-center\">Titulo</th>\r\n                        <th class=\"text-center\">Texto</th>\r\n                        <th class=\"text-center\">URL</th>\r\n                        <th></th>\r\n                    </thead>\r\n                    <tbody>\r\n                        <tr *ngFor=\"let list of listimagens | async\">\r\n                            <td class=\"td-actions text-right\">\r\n                                <button type=\"button\" rel=\"tooltip\" class=\"btn btn-success btn-sm\" data-toggle=\"modal\"\r\n                                    data-target=\"#modalimagem\"\r\n                                    (click)=\"preencherFormImagem(list.id,list.descricao,list.titulo,list.texto,list.url)\">\r\n                                    <i class=\"material-icons\">edit</i>\r\n                                </button>\r\n                            </td>\r\n                            <td class=\"text-center\" style=\"vertical-align: middle\">{{list.descricao}}</td>\r\n                            <td class=\"text-center\" style=\"vertical-align: middle\">{{list.titulo}}</td>\r\n                            <td class=\"text-center\" style=\"vertical-align: middle\">{{list.texto}}</td>\r\n                            <td class=\"text-center\" style=\"vertical-align: middle\">{{list.url}}</td>\r\n                        </tr>\r\n                    </tbody>\r\n                </table>\r\n\r\n                <!-- END TABELA DE VIDEOS -->\r\n\r\n                <br><br>\r\n            </div>\r\n        </div>\r\n        <br><br>\r\n\r\n\r\n        <!-- MODAL IMPORT IMAGEM FIRESTORAGE -->\r\n\r\n\r\n        <br><br>\r\n    </div>\r\n</div>\r\n\r\n<!-- MODAL NOVO VIDEO -->\r\n\r\n<div class=\"modal fade \" id=\"modalnovo\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"VideoModalLabel\"\r\n    aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\"></div>\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\" id=\"VideoModalLabel\">Novo Video</h5>\r\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" (click)=\"limparForm()\" aria-label=\"Fechar\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <form [formGroup]=\"form\">\r\n                    <div class=\"form-group\">\r\n                        <label for=\"video_descricao\">Descricao</label>\r\n                        <input class=\"form-control\" formControlName=\"descricao\" id=\"video_descricao\"\r\n                            name=\"video_descricao\" placeholder=\"descricao do video\" required>\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"exampleFormControlInput1\">Link</label>\r\n                        <input class=\"form-control\" formControlName=\"link\" id=\"video_link\" placeholder=\"link do video\"\r\n                            required>\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"video_categoria\">Empresa</label>\r\n                        <select class=\"form-control\" formControlName=\"empresa\" id=\"video_categoria\">\r\n                            <option>clinica</option>\r\n                            <option>comercial</option>\r\n                        </select>\r\n                    </div>\r\n\r\n                    <div class=\"form-group\">\r\n                        <label for=\"video_categoria\">Categoria</label>\r\n                        <select class=\"form-control\" formControlName=\"categoria\" id=\"video_categoria\">\r\n                            <option *ngFor=\"let cat of listcategoria | async\">{{cat.nome}}</option>\r\n                        </select>\r\n                    </div>\r\n                    <div class=\"modal-footer\">\r\n                        <button type=\"button\" class=\"btn btn-darnger btn-sm\" (click)=\"limparForm()\"\r\n                            data-dismiss=\"modal\">Fechar</button>\r\n                        ||\r\n                        <button type=\"submit\" class=\"btn btn-success btn-sm\" (click)=\"insertVideos();limparForm()\"\r\n                            data-dismiss=\"modal\">Salvar</button>\r\n                    </div>\r\n                </form>\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<!-- END MODAL NOVO VIDEO -->\r\n\r\n\r\n<!-- MODAL EDIT VIDEO -->\r\n\r\n<div class=\"modal fade \" id=\"modaleditar\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"VideoModalLabel\"\r\n    aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\"></div>\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\" id=\"VideoModalLabel\">Editar Video</h5>\r\n                <button type=\"button\" class=\"close\" (click)=\"limparForm()\" data-dismiss=\"modal\" aria-label=\"Fechar\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <form [formGroup]=\"form\">\r\n                    <div class=\"form-group\">\r\n                        <label for=\"video_descricao\">Descricao</label>\r\n                        <input class=\"form-control\" formControlName=\"descricao\" id=\"video_descricao\"\r\n                            name=\"video_descricao\" placeholder=\"descricao do video\" required>\r\n                    </div>\r\n\r\n                    <div class=\"form-group\">\r\n                        <label for=\"exampleFormControlInput1\">Link</label>\r\n                        <input class=\"form-control\" formControlName=\"link\" id=\"video_link\" placeholder=\"link do video\">\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"video_empresa\">Empresa</label>\r\n                        <select class=\"form-control\" formControlName=\"empresa\" id=\"video_empresa\">\r\n                            <option>clinica</option>\r\n                            <option>comercial</option>\r\n                        </select>\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"video_categoria\">Categoria</label>\r\n                        <select class=\"form-control\" formControlName=\"categoria\" id=\"video_categoria\">\r\n                            <option *ngFor=\"let cat of listcategoria | async\">{{cat.nome}}</option>\r\n                        </select>\r\n                    </div>\r\n                    <div class=\"modal-footer\">\r\n                        <button type=\"button\" class=\"btn btn-darnger btn-sm\" (click)=\"limparForm()\"\r\n                            data-dismiss=\"modal\">Fechar</button>\r\n                        ||\r\n                        <button type=\"button\" class=\"btn btn-success btn-sm\" (click)=\"updateVideos(); limparForm()\"\r\n                            data-dismiss=\"modal\">Salvar</button>\r\n                    </div>\r\n                </form>\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<!-- END MODAL NOVO VIDEO -->\r\n\r\n<!-- INICIO MODAL EDITA IMAGEM -->\r\n<div class=\"modal fade \" id=\"modalimagem\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"VideoModalLabel\"\r\n    aria-hidden=\"true\">\r\n\r\n\r\n\r\n\r\n\r\n    <div class=\"modal-dialog\"></div>\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\" id=\"VideoModalLabel\">Editar Imagem</h5>\r\n                <button type=\"button\" class=\"close\" (click)=\"limparForm()\" data-dismiss=\"modal\" aria-label=\"Fechar\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <form [formGroup]=\"form\">\r\n                    <div class=\"form-group\">\r\n                        <label for=\"video_descricao\">Descrição</label>\r\n                        <input class=\"form-control\" formControlName=\"descricao\" id=\"imagem_descricao\"\r\n                            name=\"imagem_descricao\" placeholder=\"Descrição Imagem\" required>\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"video_descricao\">Titulo</label>\r\n                        <input class=\"form-control\" formControlName=\"titulo\" id=\"imagem_titulo\" name=\"imagem_titulo\"\r\n                            placeholder=\"Descrição Título\" required>\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"video_descricao\">Texto</label>\r\n                        <input class=\"form-control\" formControlName=\"texto\" id=\"imagem_texto\" name=\"imagem_texto\"\r\n                            placeholder=\"Descrição Texto\" required>\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"video_descricao\">URL</label>\r\n                        <input class=\"form-control\" formControlName=\"url\" id=\"imagem_url\" name=\"imagem_url\"\r\n                            placeholder=\"URL da Imagem\" required>\r\n                    </div>\r\n\r\n\r\n\r\n                    <input type=\"file\" (change)=\"upload($event)\" accept=\".png,.jpg\" />\r\n                    <br>\r\n\r\n                    <button type=\"button\" class=\"btn btn-sucess btn-sm\" (click)=\"upload_firebase()\">Enviar</button>\r\n\r\n                    <br><br>\r\n                    <div class=\"progress\">\r\n                        <div class=\"progress-bar progress-bar-striped bg-success\" role=\"progressbar\"\r\n                            [style.width]=\"(uploadProgress | async) + '%'\"\r\n                            [attr.aria-valuenow]=\"(uploadProgress | async)\" aria-valuemin=\"0\" aria-valuemax=\"100\">\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"modal-footer\">\r\n                        <button type=\"button\" class=\"btn btn-darnger btn-sm\" (click)=\"limparForm()\"\r\n                            data-dismiss=\"modal\">Fechar</button>\r\n                        ||\r\n                        <button type=\"button\" class=\"btn btn-success btn-sm\" (click)=\"updateVideos(); limparForm()\"\r\n                            data-dismiss=\"modal\">Salvar</button>\r\n                    </div>\r\n                </form>\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n\r\n</div>\r\n\r\n\r\n<!--\r\n        \r\n<div class=\"modal fade \" id=\"modalimagem\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"VideoModalLabel\" aria-hidden=\"true\">\r\n        <div class=\"card\">\r\n            <div class=\"card-header\">\r\n                Enviar Imagens\r\n            </div>\r\n            <div class=\"card-body\">\r\n                <h5 class=\"card-title\">Select a file for upload:</h5>\r\n                <input type=\"file\" (change)=\"upload($event)\" accept=\".png,.jpg\" />\r\n                <br><br>\r\n                <div class=\"progress\">\r\n                    <div class=\"progress-bar progress-bar-striped bg-success\" role=\"progressbar\"\r\n                        [style.width]=\"(uploadProgress | async) + '%'\"\r\n                        [attr.aria-valuenow]=\"(uploadProgress | async)\" aria-valuemin=\"0\" aria-valuemax=\"100\">\r\n                    </div>\r\n                </div>\r\n                <br>\r\n                <div class=\"btn-group\" role=\"group\" *ngIf=\"uploadState | async; let state\">\r\n                    <button type=\"button\" class=\"btn btn-primary\" (click)=\"task.pause()\"\r\n                        [disabled]=\"state === 'paused'\">Pause</button>\r\n                    <button type=\"button\" class=\"btn btn-primary\" (click)=\"task.cancel()\"\r\n                        [disabled]=\"!(state === 'paused' || state ==='running')\">Cancel</button>\r\n                    <button type=\"button\" class=\"btn btn-primary\" (click)=\"task.resume()\"\r\n                        [disabled]=\"state === 'running'\">Resume</button>\r\n                </div>\r\n                <br><br>\r\n                <div *ngIf=\"downloadURL | async; let downloadSrc\" class=\"alert alert-info\" role=\"alert\">\r\n                    File uploaded: <a [href]=\"downloadSrc\">{{downloadSrc}}</a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/page-home/contato/component-contato.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/page-home/contato/component-contato.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"contactus-1 section section-image\" id=\"contato\"\r\n  style=\"background-image: url('./assets/img/profile_city.jpg')\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-5 ml-auto\">\r\n        <h2 class=\"title\">Contato</h2>\r\n        <h5 class=\"description\" style=\"color: black\">Nos temos o suporte técnico para retirar suas dúvidas e resolver\r\n          seus problemas. <br>Boa Ideia fica feliz em atende-lo.</h5>\r\n        <div class=\"row\">\r\n          <div class=\"col-md-2\">\r\n            <div class=\"icon icon-danger\" style=\"margin-top: 15px\">\r\n              <i class=\"material-icons\">pin_drop</i>\r\n            </div>\r\n          </div>\r\n          <div class=\"col\">\r\n\r\n            <div class=\"description\" style=\"color: black\">\r\n              <h4 class=\"info-title\">Encontre-nos no escritório</h4>\r\n\r\n              <p> Rua Irmã Beata, 57C\r\n                <br> 39400-101 Centro,\r\n                <br> Montes Claros - MG\r\n              </p>\r\n\r\n              <div class=\"description\" style=\"color: black\">\r\n                <a href=\"mailto:boaideia@boaideianet.com.br&amp;subject=Contato site\"\r\n                  style=\"color: black\">boaideia@boaideianet.com.br</a>\r\n              </div>\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-2\">\r\n              <div class=\"icon icon-danger\" style=\"margin-top: 15px\">\r\n                <i class=\"material-icons\">phone</i>\r\n              </div>\r\n            </div>\r\n            <div class=\"col\">\r\n              <div class=\"description\" style=\"color: black\">\r\n                <h4 class=\"info-title\">Telefone</h4>\r\n                <strong>\r\n                  <p> Recepção\r\n                    <br> +55 (38) 3221-8277\r\n                  </p>\r\n                  <p> Plantão\r\n                    <br>\r\n                    <a\r\n                      href=\"https://api.whatsapp.com/send?phone=5538984069498&text=Oi.%20Gostaria%20de%20um%20atendimento.\">\r\n                      <button class=\"btn btn-success btn-round btn-sm\">\r\n                        <i class=\"fa fa-whatsapp\"></i> &nbsp; +55 (38) 99806-9498\r\n                      </button>\r\n                    </a>\r\n                    <!--          <a class=\"nav-link\" href=\"https://api.whatsapp.com/send?phone=5538984069498&text=Oi.%20Gostaria%20de%20um%20atendimento.\" rel=\"tooltip\" title=\"\" data-placement=\"bottom\" target=\"_blank\"\r\n            data-original-title=\"Entre em contato agora\">\r\n            <i class=\"fa fa-whatsapp\"></i>\r\n          </a>-->\r\n                  </p>\r\n                </strong>\r\n\r\n\r\n\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <!--Google map-->\r\n      <div class=\"col-md-6 mb-4\">\r\n  \r\n          <!--Card-->\r\n          <div class=\"card card-cascade narrower\">\r\n      \r\n            <!--Card image-->\r\n            <div class=\"view view-cascade gradient-card-header peach-gradient\">\r\n              <h5 class=\"mb-0 margin-left-custom\">Boa Ideia Consultoria & Informatica</h5>\r\n            </div>\r\n            <!--/Card image-->\r\n      \r\n            <!--Card content-->\r\n            <div class=\"card-body card-body-cascade text-center\">\r\n      \r\n              <!--Google map-->\r\n              <div id=\"map-container-google-9\" class=\"z-depth-1-half map-container-5\" style=\"height: 400px\">\r\n                <iframe src=\"https://www.google.com/maps/embed?pb=!1m17!1m11!1m3!1d3882.000472545978!2d-43.87112857716301!3d-16.721999775745957!2m2!1f0!2f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xab535577ae1d79%3A0x61e69a475ed69c69!2sR.%20Irm%C3%A3%20Beata%2C%2057%20-%20Centro%2C%20Montes%20Claros%20-%20MG%2C%2039400-110!5e1!3m2!1spt-BR!2sbr!4v1566592135046!5m2!1spt-BR!2sbr\" frameborder=\"0\"\r\n                  style=\"border:0\" allowfullscreen></iframe>\r\n\r\n                  \r\n                  \r\n      \r\n      \r\n            </div>\r\n            <!--/.Card content-->\r\n      \r\n          </div>\r\n          <!--/.Card-->\r\n      \r\n        </div>\r\n\r\n      <!--Google Maps-->\r\n      <!--\r\n      <div class=\"col-md-5 ml-auto\">\r\n        <div class=\"card card-contact\">\r\n          <form id=\"contact-form\" method=\"post\">\r\n            <div class=\"card-header card-header-raised card-header-danger text-center\">\r\n              <h4 class=\"card-title\">Envie-nos uma mensagem</h4>\r\n            </div>\r\n            <div class=\"card-body\">\r\n              <div class=\"row\">\r\n                <div class=\"col-md-6\">\r\n                  <div class=\"form-group label-floating is-empty bmd-form-group\">\r\n                    <label class=\"bmd-label-floating\">Primeiro nome</label>\r\n                    <input type=\"text\" name=\"name\" class=\"form-control\">\r\n                    <span class=\"material-input\"></span>\r\n                  </div>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                  <div class=\"form-group label-floating is-empty bmd-form-group\">\r\n                    <label class=\"bmd-label-floating\">Último nome</label>\r\n                    <input type=\"text\" name=\"name\" class=\"form-control\">\r\n                    <span class=\"material-input\"></span>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"form-group label-floating is-empty bmd-form-group\">\r\n                <label class=\"bmd-label-floating\">Endereço de email</label>\r\n                <input type=\"email\" name=\"email\" class=\"form-control\">\r\n                <span class=\"material-input\"></span>\r\n              </div>\r\n              <div class=\"form-group label-floating is-empty bmd-form-group\">\r\n                <label for=\"exampleMessage1\" class=\"bmd-label-floating\">Sua mensagem</label>\r\n                <textarea name=\"message\" class=\"form-control\" id=\"exampleMessage1\" rows=\"6\"></textarea>\r\n                <span class=\"material-input\"></span>\r\n              </div>\r\n            </div>\r\n            <div class=\"card-footer justify-content-between\">\r\n              <div class=\"form-check\">\r\n                <label class=\"form-check-label\">\r\n                  <input class=\"form-check-input\" type=\"checkbox\" value=\"\"> Eu não sou um robô\r\n                  <span class=\"form-check-sign\">\r\n                    <span class=\"check\"></span>\r\n                  </span>\r\n                </label>\r\n              </div>\r\n              <button type=\"submit\" class=\"btn btn-danger pull-right\">ENVIAR MENSAGEM</button>\r\n            </div>\r\n          </form>\r\n        </div>\r\n      </div>\r\n    -->\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/page-home/home/home.component.html":
/*!******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/page-home/home/home.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"section page-header header-filter\" id=\"inicial\" data-parallax=\"true\"\r\n  style=\"background-image: url('../assets/img/imagens_tiago/3.jpg')\">\r\n  <div class=\"container text-center\">\r\n\r\n    <h2 class=\"title\">Soluções Práticas e Personalizadas\r\n      para você</h2>\r\n    <h4>\r\n      Nosso objetivo é levar, aos nossos clientes, nossa\r\n      expertise de quase <strong>30 anos de mercado</strong>. Com a implantação de metodologias e ferramentas de gestão\r\n      para obter a <strong>\r\n        maximização de resultados.\r\n      </strong>\r\n    </h4>\r\n    <br>\r\n\r\n\r\n  </div>\r\n</div>\r\n<div class=\"main main-raised\">\r\n\r\n\r\n  <!-- Init Freature 1 -->\r\n  <div class=\"container\">\r\n\r\n    <!-- Init Carousel Card -->\r\n    <div class=\"section\">\r\n      <!--id=\"carousel\"-->\r\n\r\n      <div class=\"container\">\r\n        <div class=\"row\">\r\n          <div class=\"col-md-8 mr-auto ml-auto\">\r\n            <!-- Carousel Card -->\r\n            <div class=\"card card-raised card-carousel\">\r\n              <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\" data-interval=\"3000\">\r\n                <ol class=\"carousel-indicators\">\r\n                  <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"0\" class=\"\"></li>\r\n                  <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"1\" class=\"\"></li>\r\n                  <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"2\" class=\"active\"></li>\r\n                </ol>\r\n                <div class=\"carousel-inner\">\r\n\r\n                  <!--IMAGEM 1 DO SLIDE-->\r\n                  <div class=\"carousel-item\">\r\n\r\n                    <ng-container>\r\n                      <img [src]=\"img_carrossel_1\" class=\"d-block w-100\" alt=\"First slide\">\r\n                    </ng-container>\r\n\r\n\r\n                    <div class=\"carousel-caption d-none d-md-block\">\r\n                      <h4>\r\n                        <i class=\"material-icons\">.</i> ..\r\n                      </h4>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <!--IMAGEM 2 DO SLIDE-->\r\n                  <div class=\"carousel-item\">\r\n                    <!--                    <img class=\"d-block w-100\" src=\"./assets/img/imagens_tiago/9.jpg\" alt=\"Second slide\">-->\r\n                    <ng-container>\r\n                      <img [src]=\"img_carrossel_2\" class=\"d-block w-100\" alt=\"Second slide\">\r\n                    </ng-container>\r\n\r\n                    <div class=\"carousel-caption d-none d-md-block\">\r\n                      <h4>\r\n                        <i class=\"material-icons\">.</i> ..\r\n                      </h4>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <!--IMAGEM 3 DO SLIDE     UM DOS SLID TEM QUE TER ESTE    'active' PARA PODER FUNCIONAR O CORROSSEL-->\r\n                  <div class=\"carousel-item active\">\r\n                    <ng-container>\r\n                      <img [src]=\"img_carrossel_3\" class=\"d-block w-100\" alt=\"Third slide\">\r\n                    </ng-container>\r\n\r\n                    <!--<img class=\"d-block w-100\" src=\"./assets/img/imagens_tiago/12.jpg\" alt=\"Third slide\">-->\r\n                    <div class=\"carousel-caption d-none d-md-block\">\r\n                      <h4>\r\n                        <i class=\"material-icons\">.</i> ..\r\n                      </h4>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">\r\n                  <i class=\"material-icons\">keyboard_arrow_left</i>\r\n                  <span class=\"sr-only\">Previous</span>\r\n                </a>\r\n                <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">\r\n                  <i class=\"material-icons\">keyboard_arrow_right</i>\r\n                  <span class=\"sr-only\">Next</span>\r\n                </a>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"section text-center\" id=\"solucoes\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-8 ml-auto mr-auto\">\r\n          <!--<h2 class=\"title\">Características sobre o produto</h2>-->\r\n          <h4 class=\"description\">\r\n            O nosso software tem como caracteristica principal\r\n            <strong>aperfeiçoar a gestão</strong> e, consequentemente\r\n            , auxiliar as instituições a atingirem seus objetivos\r\n            e metas.\r\n\r\n          </h4>\r\n        </div>\r\n      </div>\r\n      <div class=\"features\">\r\n        <h2 class=\"title\">Soluções em Software</h2>\r\n        <div class=\"row\">\r\n          <div class=\"col-md-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-success\">\r\n                <i class=\"material-icons\">opacity</i>\r\n              </div>\r\n              <h4 class=\"info-title\">Clínicas</h4>\r\n              <p></p>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-md-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-danger\">\r\n                <i class=\"material-icons\">add_comment</i>\r\n              </div>\r\n              <h4 class=\"info-title\">Hospitais</h4>\r\n              <p></p>\r\n            </div>\r\n          </div>\r\n          <div class=\"col-md-4\">\r\n            <div class=\"info\">\r\n              <div class=\"icon icon-info\">\r\n                <i class=\"material-icons\">business</i>\r\n              </div>\r\n              <h4 class=\"info-title\">Comercios</h4>\r\n              <p></p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n      <!-- Init Project -->\r\n      <div class=\"section\" id=\"projects\">\r\n        <div class=\"projects-4\" id=\"projects-4\">\r\n          <div class=\"container\">\r\n            <!--\r\n        <div class=\"row\">\r\n          <div class=\"col-md-8 ml-auto mr-auto text-center\">\r\n            <h2 class=\"title\">Características</h2>\r\n            <h5 class=\"description\"> .</h5>\r\n            <div class=\"section-space\"></div>\r\n          </div>\r\n        </div>-->\r\n            <div class=\"row\">\r\n              <div class=\"col-md-5 ml-auto\">\r\n                <div class=\"card card-background\"\r\n                  style=\"background-image:url('./assets/img/examples/clinica33.jpg');height: 330px\">\r\n                  <a href=\"#pablo\"></a>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-md-5 mr-auto\">\r\n                <div class=\"info info-horizontal\">\r\n                  <div class=\"icon icon-info\">\r\n                    <i class=\"material-icons\">format_paint</i>\r\n                  </div>\r\n                  <div class=\"description\">\r\n                    <h4 class=\"info-title\">Area Administrativa</h4>\r\n                    <p class=\"description\">\r\n                      Usuario, senha e permissões.\r\n                    </p>\r\n                  </div>\r\n                </div>\r\n                <div class=\"info info-horizontal\">\r\n                  <div class=\"icon icon-primary\">\r\n                    <i class=\"material-icons\">code</i>\r\n                  </div>\r\n                  <div class=\"description\">\r\n                    <h4 class=\"info-title\">Triagem</h4>\r\n                    <p class=\"description\">\r\n                      Manchester\r\n                    </p>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <hr>\r\n            <div class=\"row\">\r\n              <div class=\"col-md-5 ml-auto\">\r\n                <div class=\"info info-horizontal\">\r\n                  <div class=\"icon icon-rose\">\r\n                    <i class=\"material-icons\">timeline</i>\r\n                  </div>\r\n                  <div class=\"description\">\r\n                    <h4 class=\"info-title\">Laboratório</h4>\r\n                    <p class=\"description\">\r\n                      Exames.\r\n                    </p>\r\n                  </div>\r\n                </div>\r\n                <div class=\"info info-horizontal\">\r\n                  <div class=\"icon icon-success\">\r\n                    <i class=\"material-icons\">code</i>\r\n                  </div>\r\n                  <div class=\"description\">\r\n                    <h4 class=\"info-title\">Prescrição Medica </h4>\r\n                    <p class=\"description\">\r\n                      Receitas e Historico.\r\n                    </p>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-md-5 mr-auto\">\r\n                <div class=\"card card-background\"\r\n                  style=\"background-image:url('./assets/img/examples/clinica333.jpg');height: 330px\">\r\n                  <a href=\"#pablo\"></a>\r\n\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <hr>\r\n            <div class=\"row\">\r\n              <div class=\"col-md-5 ml-auto\">\r\n                <div class=\"card card-background\"\r\n                  style=\"background-image:url('./assets/img/examples/clinica33.jpg');height: 330px\">\r\n                  <a href=\"#pablo\"></a>\r\n                  <div class=\"card-body text-center\">\r\n                    <span class=\"badge badge-danger\">_</span>\r\n                    <a href=\"#pablo\">\r\n                      <h3 class=\"card-title\" style=\"color: white\">-</h3>\r\n                    </a>\r\n                    <p class=\"card-description\" style=\"color: white\">\r\n                      .\r\n                    </p>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-md-5 mr-auto\">\r\n                <div class=\"info info-horizontal\">\r\n                  <div class=\"icon icon-info\">\r\n                    <i class=\"material-icons\">format_paint</i>\r\n                  </div>\r\n                  <div class=\"description\">\r\n                    <h4 class=\"info-title\">NF / NFCe / Sintegra / SPED</h4>\r\n                    <p class=\"description\">\r\n                      Emissão de Nota Fiscal, Nota Fiscal de Consumidor Eletrônica, Geração de Sintegra e SPED.\r\n                    </p>\r\n                  </div>\r\n                </div>\r\n                <div class=\"info info-horizontal\">\r\n                  <div class=\"icon icon-primary\">\r\n                    <i class=\"material-icons\">code</i>\r\n                  </div>\r\n                  <div class=\"description\">\r\n                    <h4 class=\"info-title\">Controle de Estoque</h4>\r\n                    <p class=\"description\">\r\n                      Produto, marcas, lotes, histórico de entrada e saída.\r\n                    </p>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n\r\n    <!--PARCEIROS-->\r\n    <div *ngIf=\"showDiv\" class=\"main main-raised\">\r\n      <div class=\"section text-center\" id=\"team\">\r\n        <h2 class=\"title\">Nossos Parceiros</h2>\r\n        <div class=\"team\">\r\n          <div class=\"row\">\r\n            <div class=\"col-md-4\">\r\n              <div class=\"team-player\">\r\n                <div class=\"card card-plain\">\r\n                  <div class=\"col-md-6 ml-auto mr-auto\">\r\n                    <!--<img src=\"../assets/img/iury.jpg\" alt=\"Thumbnail Image\" class=\"img-raised rounded-circle img-fluid\">-->\r\n                    <img src=\"../assets/img/prontocor2.jpg\" alt=\"Thumbnail Image\"\r\n                      class=\"img-raised rounded-circle img-fluid\">\r\n                  </div>\r\n                  <h4 class=\"card-title\">Prontocor\r\n                    <br>\r\n                    <small class=\"card-description text-muted\">Referencial em pronto atendimento</small>\r\n                  </h4>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-4\">\r\n              <div class=\"team-player\">\r\n                <div class=\"card card-plain\">\r\n                  <div class=\"col-md-6 ml-auto mr-auto\">\r\n                    <img src=\"../assets/img/taiobeiras2.jpg\" alt=\"Thumbnail Image\"\r\n                      class=\"img-raised rounded-circle img-fluid\">\r\n                  </div>\r\n                  <h4 class=\"card-title\">Hospital Santo Antônio\r\n                    <br>\r\n                    <small class=\"card-description text-muted\">Taiobeiras</small>\r\n                  </h4>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-4\">\r\n              <div class=\"team-player\">\r\n                <div class=\"card card-plain\">\r\n                  <div class=\"col-md-6 ml-auto mr-auto\">\r\n                    <img src=\"../assets/img/brasiliademinas2.jpg\" alt=\"Thumbnail Image\"\r\n                      class=\"img-raised rounded-circle img-fluid\">\r\n                  </div>\r\n                  <h4 class=\"card-title\">Hospital Brasilia de Minas\r\n                    <br>\r\n                    <small class=\"card-description text-muted\">Brasilia de Minas</small>\r\n                  </h4>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <hr>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-4\">\r\n              <div class=\"team-player\">\r\n                <div class=\"card card-plain\">\r\n                  <div class=\"col-md-6 ml-auto mr-auto\">\r\n                    <!--<img src=\"../assets/img/iury.jpg\" alt=\"Thumbnail Image\" class=\"img-raised rounded-circle img-fluid\">-->\r\n                    <img src=\"../assets/img/alvaro.png\" alt=\"Thumbnail Image\"\r\n                      class=\"img-raised rounded-circle img-fluid\">\r\n                  </div>\r\n                  <h4 class=\"card-title\">Alvaro Auto Peças\r\n                    <br>\r\n                    <small class=\"card-description text-muted\">Referencial em Auto</small>\r\n                  </h4>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-4\">\r\n              <div class=\"team-player\">\r\n                <div class=\"card card-plain\">\r\n                  <div class=\"col-md-6 ml-auto mr-auto\">\r\n                    <img src=\"../assets/img//mitrax.png\" alt=\"Thumbnail Image\"\r\n                      class=\"img-raised rounded-circle img-fluid\">\r\n                  </div>\r\n                  <h4 class=\"card-title\">Mitrax Armações\r\n                    <br>\r\n                    <small class=\"card-description text-muted\">Armações para sua Construção</small>\r\n                  </h4>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-4\">\r\n              <div class=\"team-player\">\r\n                <div class=\"card card-plain\">\r\n                  <div class=\"col-md-6 ml-auto mr-auto\">\r\n                    <img src=\"../assets/img/sementes.jpg\" alt=\"Thumbnail Image\"\r\n                      class=\"img-raised rounded-circle img-fluid\">\r\n                  </div>\r\n                  <h4 class=\"card-title\">Sementes Tolentino\r\n                    <br>\r\n                    <small class=\"card-description text-muted\">.</small>\r\n                  </h4>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <br><br>\r\n    <div class=\"section\">\r\n\r\n      <!--id=\"carousel\"-->\r\n\r\n      <div *ngIf=\"showDiv\" class=\"container\">\r\n        <div class=\"row\">\r\n          <div class=\"col-xs-12\">\r\n            <!-- Carousel Card -->\r\n            <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\" data-interval=\"2000\">\r\n\r\n              <!--QUANTIDADE DE SLIDES-->\r\n              <!-- <ol class=\"carousel-indicators\">\r\n                <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"0\" class=\"active\"></li>\r\n                <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"1\" class=\"\"></li>\r\n                <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"2\" class=\"\"></li>\r\n              </ol>\r\n-->\r\n              <div class=\"carousel-inner\">\r\n\r\n\r\n                <!--IMAGEM 1 DO SLIDE-->\r\n                <div class=\"carousel-item \" alt=\"First slide\">\r\n                  <div class=\"row\">\r\n                    <div class=\"col\">\r\n                      <ng-container>\r\n                        <img [src]=\"img_carrossel_p\" class=\"d-block w-100\" alt=\"1 slide\">\r\n                      </ng-container>\r\n                    </div>\r\n                    <div class=\"col\">\r\n                      <ng-container>\r\n                        <img [src]=\"img_carrossel_t\" class=\"d-block w-100\" alt=\"2 slide\">\r\n                      </ng-container>\r\n                    </div>\r\n                    <div class=\"col\">\r\n                      <ng-container>\r\n                        <img [src]=\"img_carrossel_b\" class=\"d-block w-100\" alt=\"3 slide\">\r\n                      </ng-container>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <!--IMAGEM 2 DO SLIDE-->\r\n                <div class=\"carousel-item \" alt=\"Second slide\">\r\n                  <div class=\"row\">\r\n                    <div class=\"col\">\r\n                      <ng-container>\r\n                        <img [src]=\"img_carrossel_1\" class=\"d-block w-100\" alt=\"4 slide\">\r\n                      </ng-container>\r\n                    </div>\r\n                    <div class=\"col\">\r\n                      <ng-container>\r\n                        <img [src]=\"img_carrossel_2\" class=\"d-block w-100\" alt=\"5 slide\">\r\n                      </ng-container>\r\n                    </div>\r\n                    <div class=\"col\">\r\n                      <ng-container>\r\n                        <img [src]=\"img_carrossel_3\" class=\"d-block w-100\" alt=\"6 slide\">\r\n                      </ng-container>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <!--IMAGEM 3 DO SLIDE-->\r\n                <div class=\"carousel-item active\" alt=\"Third slide\">\r\n                  <div class=\"row\">\r\n                    <div class=\"col\">\r\n                      <ng-container>\r\n                        <img [src]=\"img_carrossel_1\" class=\"d-block w-100\" alt=\"7 slide\">\r\n                      </ng-container>\r\n                    </div>\r\n                    <div class=\"col\">\r\n                      <ng-container>\r\n                        <img [src]=\"img_carrossel_2\" class=\"d-block w-100\" alt=\"8 slide\">\r\n                      </ng-container>\r\n                    </div>\r\n                    <div class=\"col\">\r\n                      <ng-container>\r\n                        <img [src]=\"img_carrossel_3\" class=\"d-block w-100\" alt=\"9 slide\">\r\n                      </ng-container>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n\r\n\r\n\r\n              </div>\r\n              <!--\r\n              <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">\r\n                <i class=\"material-icons\">keyboard_arrow_left</i>\r\n                <span class=\"sr-only\">Previous</span>\r\n              </a>\r\n              <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">\r\n                <i class=\"material-icons\">keyboard_arrow_right</i>\r\n                <span class=\"sr-only\">Next</span>\r\n              </a>-->\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n    </div>\r\n    <br><br>\r\n    <!-- End Time -->\r\n\r\n\r\n    <!-- Finish Section AlbumCard -->\r\n\r\n  </div>\r\n\r\n  <!-- Init Section Contato -->\r\n  <app-component-contato></app-component-contato>\r\n\r\n  <!-- End Section Contato -->\r\n\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/page-home/parceiros/component-parceiros.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/page-home/parceiros/component-parceiros.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"section page-header header-filter\" id=\"inicial\" data-parallax=\"true\"\r\n    style=\"background-image: url('../assets/img/imagens_tiago/20.png')\">\r\n    <div class=\"container text-center\">\r\n\r\n        <h1 class=\"title\">Nossos Parceiros</h1>\r\n        <h4>_</h4>\r\n        <br>\r\n\r\n\r\n    </div>\r\n</div>\r\n<div class=\"main main-raised\">\r\n    <div class=\"section text-center\" id=\"team\">\r\n        <h2 class=\"title\">Nossos Parceiros</h2>\r\n        <div class=\"team\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"team-player\">\r\n                        <div class=\"card card-plain\">\r\n                            <div class=\"col-md-6 ml-auto mr-auto\">\r\n                                <!--<img src=\"../assets/img/iury.jpg\" alt=\"Thumbnail Image\" class=\"img-raised rounded-circle img-fluid\">-->\r\n                                <img src=\"../assets/img/prontocor2.jpg\" alt=\"Thumbnail Image\"\r\n                                    class=\"img-raised rounded-circle img-fluid\">\r\n                            </div>\r\n                            <h4 class=\"card-title\">Prontocor\r\n                                <br>\r\n                                <small class=\"card-description text-muted\">Referencial em pronto atendimento</small>\r\n                            </h4>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"team-player\">\r\n                        <div class=\"card card-plain\">\r\n                            <div class=\"col-md-6 ml-auto mr-auto\">\r\n                                <img src=\"../assets/img/taiobeiras2.jpg\" alt=\"Thumbnail Image\"\r\n                                    class=\"img-raised rounded-circle img-fluid\">\r\n                            </div>\r\n                            <h4 class=\"card-title\">Hospital Santo Antônio\r\n                                <br>\r\n                                <small class=\"card-description text-muted\">Taiobeiras</small>\r\n                            </h4>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"team-player\">\r\n                        <div class=\"card card-plain\">\r\n                            <div class=\"col-md-6 ml-auto mr-auto\">\r\n                                <img src=\"../assets/img/brasiliademinas2.jpg\" alt=\"Thumbnail Image\"\r\n                                    class=\"img-raised rounded-circle img-fluid\">\r\n                            </div>\r\n                            <h4 class=\"card-title\">Hospital Brasilia de Minas\r\n                                <br>\r\n                                <small class=\"card-description text-muted\">Brasilia de Minas</small>\r\n                            </h4>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <hr>\r\n            <div class=\"row\">\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"team-player\">\r\n                        <div class=\"card card-plain\">\r\n                            <div class=\"col-md-6 ml-auto mr-auto\">\r\n                                <!--<img src=\"../assets/img/iury.jpg\" alt=\"Thumbnail Image\" class=\"img-raised rounded-circle img-fluid\">-->\r\n                                <img src=\"../assets/img/alvaro.png\" alt=\"Thumbnail Image\"\r\n                                    class=\"img-raised rounded-circle img-fluid\">\r\n                            </div>\r\n                            <h4 class=\"card-title\">Alvaro Auto Peças\r\n                                <br>\r\n                                <small class=\"card-description text-muted\">Referencial em Auto</small>\r\n                            </h4>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"team-player\">\r\n                        <div class=\"card card-plain\">\r\n                            <div class=\"col-md-6 ml-auto mr-auto\">\r\n                                <img src=\"../assets/img//mitrax.png\" alt=\"Thumbnail Image\"\r\n                                    class=\"img-raised rounded-circle img-fluid\">\r\n                            </div>\r\n                            <h4 class=\"card-title\">Mitrax Armações\r\n                                <br>\r\n                                <small class=\"card-description text-muted\">Armações para sua Construção</small>\r\n                            </h4>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-md-4\">\r\n                    <div class=\"team-player\">\r\n                        <div class=\"card card-plain\">\r\n                            <div class=\"col-md-6 ml-auto mr-auto\">\r\n                                <img src=\"../assets/img/sementes.jpg\" alt=\"Thumbnail Image\"\r\n                                    class=\"img-raised rounded-circle img-fluid\">\r\n                            </div>\r\n                            <h4 class=\"card-title\">Sementes Tolentino\r\n                                <br>\r\n                                <small class=\"card-description text-muted\">.</small>\r\n                            </h4>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n<br>\r\n<br>\r\n<br>\r\n<br>\r\n        <div class=\"container\">\r\n            <div class=\"row\">\r\n                <div class=\"col-xs-12\">\r\n                    <!-- Carousel Card -->\r\n                    <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\"\r\n                        data-interval=\"2000\">\r\n\r\n                        <!--QUANTIDADE DE SLIDES-->\r\n                        <!-- <ol class=\"carousel-indicators\">\r\n            <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"0\" class=\"active\"></li>\r\n            <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"1\" class=\"\"></li>\r\n            <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"2\" class=\"\"></li>\r\n          </ol>\r\n-->\r\n                        <div class=\"carousel-inner\">\r\n\r\n\r\n                            <!--IMAGEM 1 DO SLIDE-->\r\n                            <div class=\"carousel-item \" alt=\"First slide\">\r\n                                <div class=\"row\">\r\n                                    <div class=\"col\">\r\n                                        <ng-container>\r\n                                            <img [src]=\"img_carrossel_p\" class=\"d-block w-100\" alt=\"1 slide\">\r\n                                        </ng-container>\r\n                                    </div>\r\n                                    <div class=\"col\">\r\n                                        <ng-container>\r\n                                            <img [src]=\"img_carrossel_t\" class=\"d-block w-100\" alt=\"2 slide\">\r\n                                        </ng-container>\r\n                                    </div>\r\n                                    <div class=\"col\">\r\n                                        <ng-container>\r\n                                            <img [src]=\"img_carrossel_b\" class=\"d-block w-100\" alt=\"3 slide\">\r\n                                        </ng-container>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <!--IMAGEM 2 DO SLIDE-->\r\n                            <div class=\"carousel-item \" alt=\"Second slide\">\r\n                                <div class=\"row\">\r\n                                    <div class=\"col\">\r\n                                        <ng-container>\r\n                                            <img [src]=\"img_carrossel_1\" class=\"d-block w-100\" alt=\"4 slide\">\r\n                                        </ng-container>\r\n                                    </div>\r\n                                    <div class=\"col\">\r\n                                        <ng-container>\r\n                                            <img [src]=\"img_carrossel_2\" class=\"d-block w-100\" alt=\"5 slide\">\r\n                                        </ng-container>\r\n                                    </div>\r\n                                    <div class=\"col\">\r\n                                        <ng-container>\r\n                                            <img [src]=\"img_carrossel_3\" class=\"d-block w-100\" alt=\"6 slide\">\r\n                                        </ng-container>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <!--IMAGEM 3 DO SLIDE-->\r\n                            <div class=\"carousel-item active\" alt=\"Third slide\">\r\n                                <div class=\"row\">\r\n                                    <div class=\"col\">\r\n                                        <ng-container>\r\n                                            <img [src]=\"img_carrossel_1\" class=\"d-block w-100\" alt=\"7 slide\">\r\n                                        </ng-container>\r\n                                    </div>\r\n                                    <div class=\"col\">\r\n                                        <ng-container>\r\n                                            <img [src]=\"img_carrossel_2\" class=\"d-block w-100\" alt=\"8 slide\">\r\n                                        </ng-container>\r\n                                    </div>\r\n                                    <div class=\"col\">\r\n                                        <ng-container>\r\n                                            <img [src]=\"img_carrossel_3\" class=\"d-block w-100\" alt=\"9 slide\">\r\n                                        </ng-container>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n\r\n\r\n\r\n\r\n                        </div>\r\n                        <!--\r\n          <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">\r\n            <i class=\"material-icons\">keyboard_arrow_left</i>\r\n            <span class=\"sr-only\">Previous</span>\r\n          </a>\r\n          <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">\r\n            <i class=\"material-icons\">keyboard_arrow_right</i>\r\n            <span class=\"sr-only\">Next</span>\r\n          </a>-->\r\n\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/page-login/login/login.component.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/page-login/login/login.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"page-header header-filter\" data-parallax=\"true\"\r\n    style=\"background-image: url('../assets/img/profile_city.jpg')\">\r\n    <div class=\"container\">\r\n\r\n      <div class=\"row\">\r\n        <div class=\"col-lg-5 col-md-6 col-sm-8 ml-auto mr-auto\">\r\n          <form class=\"form\" method=\"\" action=\"\">\r\n            <div class=\"card card-login card-hidden\">\r\n              <div class=\"card-header card-header-danger text-center\">\r\n                <h4 class=\"card-title\">Login</h4>\r\n              </div>\r\n              <div class=\"card-body \">\r\n                <span class=\"bmd-form-group\">\r\n                  <br>\r\n                  <div class=\"input-group\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\">\r\n                        <i class=\"material-icons\">email</i>\r\n                      </span>\r\n                    </div>\r\n                    <input type=\"text\" class=\"form-control\" placeholder=\"Usuário\" [(ngModel)]=\"usuario\" name=\"usuario\" required>\r\n                  </div>\r\n                </span>\r\n                <span class=\"bmd-form-group\">\r\n                  <div class=\"input-group\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\">\r\n                        <i class=\"material-icons\">lock_outline</i>\r\n                      </span>\r\n                    </div>\r\n                    <input type=\"password\" class=\"form-control\"  [(ngModel)]=\"password\" name=\"password\" placeholder=\"Senha\" required>\r\n                  </div>\r\n                </span>\r\n              </div>\r\n              <div class=\"card-footer justify-content-center\">\r\n                <a class=\"btn btn-danger btn-link btn-lg\" (click)=\"logOn()\">Entrar</a>\r\n              </div>\r\n              <br>\r\n            </div>\r\n          </form>\r\n        </div>\r\n      </div>\r\n </div>\r\n</div>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/tutoriais/tutoriais.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/tutoriais/tutoriais.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"page-header header-filter\" data-parallax=\"true\"\r\n    style=\"background-image: url('../assets/img/clinica1.jpg');height: 200px\">\r\n    <div class=\"container\">\r\n\r\n    </div>\r\n</div>\r\n<div class=\"main main-raised\">\r\n    <div class=\"container\">\r\n\r\n        <!-- Titulo -->\r\n\r\n\r\n        <div class=\"section text-center\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-8 ml-auto mr-auto\">\r\n                    <h2 class=\"title\">Vídeos Tutorias Boa Ideia</h2>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <!-- End Titulo -->\r\n\r\n        <!-- Init Tab -->\r\n\r\n        <div class=\"card card-nav-tabs\">\r\n            <div class=\"card-header card-header-danger\">\r\n                <!-- colors: \"header-primary\", \"header-info\", \"header-success\", \"header-warning\", \"header-danger\" -->\r\n                <div class=\"nav-tabs-navigation\">\r\n                    <div class=\"nav-tabs-wrapper\">\r\n                        <ul class=\"nav nav-tabs\" data-tabs=\"tabs\">\r\n                            <li class=\"nav-item\">\r\n                                <a class=\"nav-link active\" href=\"#comercial\"\r\n                                    (click)=\"categoriaSelecionada('Comercial');filtroCategoriaSelecao('Todos','Comercial')\"\r\n                                    data-toggle=\"tab\">\r\n                                    <i class=\"material-icons\">face</i> Comercial\r\n                                </a>\r\n                            </li>\r\n                            <li class=\"nav-item\">\r\n                                <a class=\"nav-link\" href=\"#clinica\"\r\n                                    (click)=\"categoriaSelecionada('Clínica');filtroCategoriaSelecao('Todos','Clínica')\"\r\n                                    data-toggle=\"tab\">\r\n                                    <i class=\"material-icons\">chat</i> Clinica / Hospital\r\n                                    <div class=\"ripple-container\"></div></a>\r\n                            </li>\r\n                            <div class=\"form-inline ml-auto\">\r\n                                <label for=\"exampleInputEmail1\">Filtre por uma categoria: </label>\r\n                                <li class=\"nav-item dropdown\">\r\n                                    <a class=\"nav-link dropdown-toggle\" data-toggle=\"dropdown\" href=\"#0\" role=\"button\"\r\n                                        aria-haspopup=\"true\" aria-expanded=\"false\">{{categoriaModelo}}</a>\r\n                                    <div class=\"dropdown-menu\">\r\n                                        <!--\r\n                                        <a (click)=\"filtroCategoriaSelecao('')\" class=\"dropdown-item\">Todos</a>\r\n                                        <a *ngFor=\"let cat of categoria | async\"\r\n                                            (click)=\"filtroCategoriaSelecao(cat.nome)\"\r\n                                            class=\"dropdown-item\">{{cat.nome}}</a>-->\r\n                                        <a class=\"dropdown-item\">Todos</a>\r\n                                    </div>\r\n                                </li>\r\n                            </div>\r\n                        </ul>\r\n\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"card-body \">\r\n                <div class=\"tab-content text-center\">\r\n\r\n                    <div class=\"ml-auto\">\r\n                        <span class=\"nav-item\">\r\n                            <input type=\"text\" class=\"form-control\" id=\"pesquisa\" placeholder=\"Pesquise por um video\">\r\n                        </span>\r\n                    </div>\r\n\r\n                    <!-- Tab Videos Comercial -->\r\n\r\n                    <div class=\"tab-pane active\" id=\"comercial\">\r\n\r\n                        <div class=\"tab-content tab-space\">\r\n\r\n\r\n                            <div class=\"row\">\r\n                                <ng-container *ngFor=\"let post of posts | async\">\r\n                                    <div class=\"col-md-4\">\r\n                                        <div class=\"card card-profile\">\r\n                                            <div class=\"card-header card-header-image\">\r\n                                                <a href=\"#pablo\">\r\n                                                    <div class=\"embed-responsive embed-responsive-16by9\">\r\n                                                        <iframe class=\"embed-responsive-item\"\r\n                                                            [src]=\"this.sanitizer.bypassSecurityTrustResourceUrl(post.link)\"\r\n                                                            allowfullscreen></iframe>\r\n                                                    </div>\r\n                                                </a>\r\n                                            </div>\r\n                                            <div class=\"card-body \">\r\n                                                <h4 class=\"card-title\">{{post.descricao}}</h4>\r\n                                                <h6 class=\"card-category text-gray\">{{post.categoria}}</h6>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </ng-container>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n\r\n\r\n                    <!-- Tab Videos Clinica/Hospital -->\r\n                    <div class=\"tab-pane\" id=\"clinica\">\r\n\r\n                        <div class=\"tab-content tab-space\">\r\n\r\n                            <!-- Categorias Cadastros -->\r\n\r\n                            <div class=\"row\">\r\n                                <ng-container *ngFor=\"let post of posts1 | async\">\r\n                                    <div class=\"col-md-4\">\r\n                                        <div class=\"card card-profile\">\r\n                                            <div class=\"card-header card-header-image\">\r\n                                                <a href=\"#pablo\">\r\n                                                    <div class=\"embed-responsive embed-responsive-16by9\">\r\n                                                        <iframe class=\"embed-responsive-item\"\r\n                                                            [src]=\"this.sanitizer.bypassSecurityTrustResourceUrl(post.link)\"\r\n                                                            allowfullscreen></iframe>\r\n                                                    </div>\r\n                                                </a>\r\n                                            </div>\r\n                                            <div class=\"card-body \">\r\n                                                <h4 class=\"card-title\">{{post.descricao}}</h4>\r\n                                                <h6 class=\"card-category text-gray\">{{post.categoria}}</h6>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </ng-container>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <br>\r\n        <br>\r\n    </div>\r\n\r\n\r\n</div>"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _pages_tutoriais_tutoriais_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pages/tutoriais/tutoriais.component */ "./src/app/pages/tutoriais/tutoriais.component.ts");
/* harmony import */ var _page_home_home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./page-home/home/home.component */ "./src/app/page-home/home/home.component.ts");
/* harmony import */ var _page_formvideos_list_list_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./page-formvideos/list/list.component */ "./src/app/page-formvideos/list/list.component.ts");
/* harmony import */ var _page_home_parceiros_component_parceiros_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./page-home/parceiros/component-parceiros.component */ "./src/app/page-home/parceiros/component-parceiros.component.ts");
/* harmony import */ var _page_login_login_login_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./page-login/login/login.component */ "./src/app/page-login/login/login.component.ts");
/* harmony import */ var _desc_med_desc_med_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./desc-med/desc-med.component */ "./src/app/desc-med/desc-med.component.ts");
/* harmony import */ var _desc_comercial_desc_comercial_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./desc-comercial/desc-comercial.component */ "./src/app/desc-comercial/desc-comercial.component.ts");










var routes = [
    { path: '', component: _page_home_home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"] },
    { path: 'tutoriais', component: _pages_tutoriais_tutoriais_component__WEBPACK_IMPORTED_MODULE_3__["TutoriaisComponent"] },
    { path: 'home', component: _page_home_home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"] },
    { path: 'formvideos', component: _page_formvideos_list_list_component__WEBPACK_IMPORTED_MODULE_5__["ListComponent"] },
    { path: 'parceiros', component: _page_home_parceiros_component_parceiros_component__WEBPACK_IMPORTED_MODULE_6__["ComponentParceirosComponent"] },
    { path: 'login', component: _page_login_login_login_component__WEBPACK_IMPORTED_MODULE_7__["LoginComponent"] },
    { path: 'desc_med', component: _desc_med_desc_med_component__WEBPACK_IMPORTED_MODULE_8__["DescMedComponent"] },
    { path: 'desc_comercial', component: _desc_comercial_desc_comercial_component__WEBPACK_IMPORTED_MODULE_9__["DescComercialComponent"] },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule.prototype.onActivate = function (event) {
        console.log("OI");
        /*console.log(event);
    console.log("hello");
      
        let scrollToTop = window.setInterval(() => {
          let pos = window.pageYOffset;
          if (pos > 0) {
            window.scrollTo(0, pos - 1); // how far to scroll on each step
          } else {
            window.clearInterval(scrollToTop);
          }
        }, 16);*/
    };
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, {
                    scrollPositionRestoration: 'enabled'
                })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".margin-custom-logo {\r\n    margin-left: 10px;\r\n}\r\n\r\n\r\n.align-middle {\r\n    padding-bottom: 10px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxpQkFBaUI7QUFDckI7OztBQUdBO0lBQ0ksb0JBQW9CO0FBQ3hCIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFyZ2luLWN1c3RvbS1sb2dvIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG59XHJcblxyXG5cclxuLmFsaWduLW1pZGRsZSB7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _page_login_login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./page-login/login.service */ "./src/app/page-login/login.service.ts");



var AppComponent = /** @class */ (function () {
    function AppComponent(loginService) {
        var _this = this;
        this.loginService = loginService;
        this.title = 'boaideia';
        this.name_home = ['Boa Ideia', 'Boa Ideia Consultoria & Informatica', 'Consultoria & Informatica'];
        this.loginService.user.subscribe(function (user) {
            if (!user) {
                _this.isLoggin = false;
            }
            else {
                _this.isLoggin = true;
            }
        });
    }
    AppComponent.prototype.ngOnInit = function () {
        this.home1 = this.name_home[0];
        this.home2 = "";
    };
    AppComponent.prototype.modifica_texto = function () { };
    AppComponent.ctorParameters = function () { return [
        { type: _page_login_login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"] }
    ]; };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _pages_tutoriais_tutoriais_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pages/tutoriais/tutoriais.component */ "./src/app/pages/tutoriais/tutoriais.component.ts");
/* harmony import */ var _page_home_home_home_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./page-home/home/home.component */ "./src/app/page-home/home/home.component.ts");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(angularfire2_firestore__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var angularfire2_storage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angularfire2/storage */ "./node_modules/angularfire2/storage/index.js");
/* harmony import */ var angularfire2_storage__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(angularfire2_storage__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var angularfire2__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! angularfire2 */ "./node_modules/angularfire2/index.js");
/* harmony import */ var angularfire2__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(angularfire2__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _pages_tutoriais_tutorial_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./pages/tutoriais/tutorial.service */ "./src/app/pages/tutoriais/tutorial.service.ts");
/* harmony import */ var _pages_tutoriais_tutorial_directive__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./pages/tutoriais/tutorial.directive */ "./src/app/pages/tutoriais/tutorial.directive.ts");
/* harmony import */ var _page_formvideos_list_list_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./page-formvideos/list/list.component */ "./src/app/page-formvideos/list/list.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _guard_auth_guard__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./guard/auth.guard */ "./src/app/guard/auth.guard.ts");
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! angularfire2/auth */ "./node_modules/angularfire2/auth/index.js");
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(angularfire2_auth__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/esm5/autocomplete.es5.js");
/* harmony import */ var _angular_material_badge__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/badge */ "./node_modules/@angular/material/esm5/badge.es5.js");
/* harmony import */ var _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/bottom-sheet */ "./node_modules/@angular/material/esm5/bottom-sheet.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material/button-toggle */ "./node_modules/@angular/material/esm5/button-toggle.es5.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm5/card.es5.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _angular_material_chips__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/material/chips */ "./node_modules/@angular/material/esm5/chips.es5.js");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/material/stepper */ "./node_modules/@angular/material/esm5/stepper.es5.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @angular/material/divider */ "./node_modules/@angular/material/esm5/divider.es5.js");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/esm5/expansion.es5.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/esm5/grid-list.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/esm5/list.es5.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/esm5/core.es5.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm5/paginator.es5.js");
/* harmony import */ var _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! @angular/material/progress-bar */ "./node_modules/@angular/material/esm5/progress-bar.es5.js");
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! @angular/material/progress-spinner */ "./node_modules/@angular/material/esm5/progress-spinner.es5.js");
/* harmony import */ var _angular_material_radio__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! @angular/material/radio */ "./node_modules/@angular/material/esm5/radio.es5.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! @angular/material/sidenav */ "./node_modules/@angular/material/esm5/sidenav.es5.js");
/* harmony import */ var _angular_material_slider__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! @angular/material/slider */ "./node_modules/@angular/material/esm5/slider.es5.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! @angular/material/snack-bar */ "./node_modules/@angular/material/esm5/snack-bar.es5.js");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! @angular/material/sort */ "./node_modules/@angular/material/esm5/sort.es5.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/esm5/tabs.es5.js");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! @angular/material/toolbar */ "./node_modules/@angular/material/esm5/toolbar.es5.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/esm5/tooltip.es5.js");
/* harmony import */ var _angular_material_tree__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! @angular/material/tree */ "./node_modules/@angular/material/esm5/tree.es5.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _page_login_login_login_component__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ./page-login/login/login.component */ "./src/app/page-login/login/login.component.ts");
/* harmony import */ var _page_login_login_service__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ./page-login/login.service */ "./src/app/page-login/login.service.ts");
/* harmony import */ var _desc_med_desc_med_component__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! ./desc-med/desc-med.component */ "./src/app/desc-med/desc-med.component.ts");
/* harmony import */ var _desc_comercial_desc_comercial_component__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(/*! ./desc-comercial/desc-comercial.component */ "./src/app/desc-comercial/desc-comercial.component.ts");
/* harmony import */ var _page_home_parceiros_component_parceiros_component__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(/*! ./page-home/parceiros/component-parceiros.component */ "./src/app/page-home/parceiros/component-parceiros.component.ts");
/* harmony import */ var _page_home_contato_component_contato_component__WEBPACK_IMPORTED_MODULE_57__ = __webpack_require__(/*! ./page-home/contato/component-contato.component */ "./src/app/page-home/contato/component-contato.component.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_58__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/footer/footer.component.ts");



























































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _pages_tutoriais_tutoriais_component__WEBPACK_IMPORTED_MODULE_5__["TutoriaisComponent"],
                _page_home_home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"],
                _pages_tutoriais_tutorial_directive__WEBPACK_IMPORTED_MODULE_12__["TutorialDirective"],
                _page_formvideos_list_list_component__WEBPACK_IMPORTED_MODULE_13__["ListComponent"],
                _page_login_login_login_component__WEBPACK_IMPORTED_MODULE_52__["LoginComponent"],
                _desc_med_desc_med_component__WEBPACK_IMPORTED_MODULE_54__["DescMedComponent"],
                _desc_comercial_desc_comercial_component__WEBPACK_IMPORTED_MODULE_55__["DescComercialComponent"],
                _page_home_parceiros_component_parceiros_component__WEBPACK_IMPORTED_MODULE_56__["ComponentParceirosComponent"],
                _page_home_contato_component_contato_component__WEBPACK_IMPORTED_MODULE_57__["ComponentContatoComponent"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_58__["FooterComponent"],
            ],
            imports: [
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_51__["BrowserAnimationsModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                angularfire2__WEBPACK_IMPORTED_MODULE_9__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].firebase),
                angularfire2_firestore__WEBPACK_IMPORTED_MODULE_7__["AngularFirestoreModule"],
                angularfire2_storage__WEBPACK_IMPORTED_MODULE_8__["AngularFireStorageModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_14__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_14__["ReactiveFormsModule"],
                _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_17__["MatAutocompleteModule"],
                _angular_material_badge__WEBPACK_IMPORTED_MODULE_18__["MatBadgeModule"],
                _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_19__["MatBottomSheetModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_20__["MatButtonModule"],
                _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_21__["MatButtonToggleModule"],
                _angular_material_card__WEBPACK_IMPORTED_MODULE_22__["MatCardModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_23__["MatCheckboxModule"],
                _angular_material_chips__WEBPACK_IMPORTED_MODULE_24__["MatChipsModule"],
                _angular_material_stepper__WEBPACK_IMPORTED_MODULE_25__["MatStepperModule"],
                _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_26__["MatDatepickerModule"],
                _angular_material_dialog__WEBPACK_IMPORTED_MODULE_27__["MatDialogModule"],
                _angular_material_divider__WEBPACK_IMPORTED_MODULE_28__["MatDividerModule"],
                _angular_material_expansion__WEBPACK_IMPORTED_MODULE_29__["MatExpansionModule"],
                _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_30__["MatGridListModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_31__["MatIconModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_32__["MatInputModule"],
                _angular_material_list__WEBPACK_IMPORTED_MODULE_33__["MatListModule"],
                _angular_material_menu__WEBPACK_IMPORTED_MODULE_34__["MatMenuModule"],
                _angular_material_core__WEBPACK_IMPORTED_MODULE_35__["MatNativeDateModule"],
                _angular_material_paginator__WEBPACK_IMPORTED_MODULE_36__["MatPaginatorModule"],
                _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_37__["MatProgressBarModule"],
                _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_38__["MatProgressSpinnerModule"],
                _angular_material_radio__WEBPACK_IMPORTED_MODULE_39__["MatRadioModule"],
                _angular_material_core__WEBPACK_IMPORTED_MODULE_35__["MatRippleModule"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_40__["MatSelectModule"],
                _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_41__["MatSidenavModule"],
                _angular_material_slider__WEBPACK_IMPORTED_MODULE_42__["MatSliderModule"],
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_43__["MatSlideToggleModule"],
                _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_44__["MatSnackBarModule"],
                _angular_material_sort__WEBPACK_IMPORTED_MODULE_45__["MatSortModule"],
                _angular_material_table__WEBPACK_IMPORTED_MODULE_46__["MatTableModule"],
                _angular_material_tabs__WEBPACK_IMPORTED_MODULE_47__["MatTabsModule"],
                _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_48__["MatToolbarModule"],
                _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_49__["MatTooltipModule"],
                _angular_material_tree__WEBPACK_IMPORTED_MODULE_50__["MatTreeModule"],
            ],
            exports: [
                _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_17__["MatAutocompleteModule"],
                _angular_material_badge__WEBPACK_IMPORTED_MODULE_18__["MatBadgeModule"],
                _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_19__["MatBottomSheetModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_20__["MatButtonModule"],
                _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_21__["MatButtonToggleModule"],
                _angular_material_card__WEBPACK_IMPORTED_MODULE_22__["MatCardModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_23__["MatCheckboxModule"],
                _angular_material_chips__WEBPACK_IMPORTED_MODULE_24__["MatChipsModule"],
                _angular_material_stepper__WEBPACK_IMPORTED_MODULE_25__["MatStepperModule"],
                _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_26__["MatDatepickerModule"],
                _angular_material_dialog__WEBPACK_IMPORTED_MODULE_27__["MatDialogModule"],
                _angular_material_divider__WEBPACK_IMPORTED_MODULE_28__["MatDividerModule"],
                _angular_material_expansion__WEBPACK_IMPORTED_MODULE_29__["MatExpansionModule"],
                _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_30__["MatGridListModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_31__["MatIconModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_32__["MatInputModule"],
                _angular_material_list__WEBPACK_IMPORTED_MODULE_33__["MatListModule"],
                _angular_material_menu__WEBPACK_IMPORTED_MODULE_34__["MatMenuModule"],
                _angular_material_core__WEBPACK_IMPORTED_MODULE_35__["MatNativeDateModule"],
                _angular_material_paginator__WEBPACK_IMPORTED_MODULE_36__["MatPaginatorModule"],
                _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_37__["MatProgressBarModule"],
                _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_38__["MatProgressSpinnerModule"],
                _angular_material_radio__WEBPACK_IMPORTED_MODULE_39__["MatRadioModule"],
                _angular_material_core__WEBPACK_IMPORTED_MODULE_35__["MatRippleModule"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_40__["MatSelectModule"],
                _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_41__["MatSidenavModule"],
                _angular_material_slider__WEBPACK_IMPORTED_MODULE_42__["MatSliderModule"],
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_43__["MatSlideToggleModule"],
                _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_44__["MatSnackBarModule"],
                _angular_material_sort__WEBPACK_IMPORTED_MODULE_45__["MatSortModule"],
                _angular_material_table__WEBPACK_IMPORTED_MODULE_46__["MatTableModule"],
                _angular_material_tabs__WEBPACK_IMPORTED_MODULE_47__["MatTabsModule"],
                _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_48__["MatToolbarModule"],
                _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_49__["MatTooltipModule"],
                _angular_material_tree__WEBPACK_IMPORTED_MODULE_50__["MatTreeModule"],
            ],
            providers: [_pages_tutoriais_tutorial_service__WEBPACK_IMPORTED_MODULE_11__["TutorialService"], _page_login_login_service__WEBPACK_IMPORTED_MODULE_53__["LoginService"], _guard_auth_guard__WEBPACK_IMPORTED_MODULE_15__["AuthGuard"], angularfire2__WEBPACK_IMPORTED_MODULE_9__["AngularFireModule"], angularfire2_auth__WEBPACK_IMPORTED_MODULE_16__["AngularFireAuth"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/desc-comercial/desc-comercial.component.css":
/*!*************************************************************!*\
  !*** ./src/app/desc-comercial/desc-comercial.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rlc2MtY29tZXJjaWFsL2Rlc2MtY29tZXJjaWFsLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/desc-comercial/desc-comercial.component.ts":
/*!************************************************************!*\
  !*** ./src/app/desc-comercial/desc-comercial.component.ts ***!
  \************************************************************/
/*! exports provided: DescComercialComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DescComercialComponent", function() { return DescComercialComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var DescComercialComponent = /** @class */ (function () {
    function DescComercialComponent() {
    }
    DescComercialComponent.prototype.ngOnInit = function () {
    };
    DescComercialComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-desc-comercial',
            template: __webpack_require__(/*! raw-loader!./desc-comercial.component.html */ "./node_modules/raw-loader/index.js!./src/app/desc-comercial/desc-comercial.component.html"),
            styles: [__webpack_require__(/*! ./desc-comercial.component.css */ "./src/app/desc-comercial/desc-comercial.component.css")]
        })
    ], DescComercialComponent);
    return DescComercialComponent;
}());



/***/ }),

/***/ "./src/app/desc-med/desc-med.component.css":
/*!*************************************************!*\
  !*** ./src/app/desc-med/desc-med.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rlc2MtbWVkL2Rlc2MtbWVkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/desc-med/desc-med.component.ts":
/*!************************************************!*\
  !*** ./src/app/desc-med/desc-med.component.ts ***!
  \************************************************/
/*! exports provided: DescMedComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DescMedComponent", function() { return DescMedComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var DescMedComponent = /** @class */ (function () {
    function DescMedComponent() {
        //IRA OCULTAR UMA DIV NO TEMPLATE
        //USANDO DA DIRETIVA *ngIf
        this.showDiv = false;
    }
    DescMedComponent.prototype.ngOnInit = function () {
    };
    DescMedComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-desc-med',
            template: __webpack_require__(/*! raw-loader!./desc-med.component.html */ "./node_modules/raw-loader/index.js!./src/app/desc-med/desc-med.component.html"),
            styles: [__webpack_require__(/*! ./desc-med.component.css */ "./src/app/desc-med/desc-med.component.css")]
        })
    ], DescMedComponent);
    return DescMedComponent;
}());



/***/ }),

/***/ "./src/app/footer/footer.component.css":
/*!*********************************************!*\
  !*** ./src/app/footer/footer.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".min-text {\r\n    font-size: 13px;\r\n}\r\n\r\n.margin-bot {\r\n    margin-bottom: 4px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZm9vdGVyL2Zvb3Rlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksZUFBZTtBQUNuQjs7QUFFQTtJQUNJLGtCQUFrQjtBQUN0QiIsImZpbGUiOiJzcmMvYXBwL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5taW4tdGV4dCB7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbn1cclxuXHJcbi5tYXJnaW4tYm90IHtcclxuICAgIG1hcmdpbi1ib3R0b206IDRweDtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/footer/footer.component.ts":
/*!********************************************!*\
  !*** ./src/app/footer/footer.component.ts ***!
  \********************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! raw-loader!./footer.component.html */ "./node_modules/raw-loader/index.js!./src/app/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.css */ "./src/app/footer/footer.component.css")]
        })
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/guard/auth.guard.ts":
/*!*************************************!*\
  !*** ./src/app/guard/auth.guard.ts ***!
  \*************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var AuthGuard = /** @class */ (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        //logic here
        console.log("VERIFY");
        console.log(localStorage['token']);
        if (localStorage['token'] == undefined) {
            this.router.navigate(['/login']);
        }
        if (localStorage['token'] != 'null') {
            return true;
        }
        else {
            this.router.navigate(['/login']);
        }
    };
    AuthGuard.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        })
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/page-formvideos/list/list.component.css":
/*!*********************************************************!*\
  !*** ./src/app/page-formvideos/list/list.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2UtZm9ybXZpZGVvcy9saXN0L2xpc3QuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/page-formvideos/list/list.component.ts":
/*!********************************************************!*\
  !*** ./src/app/page-formvideos/list/list.component.ts ***!
  \********************************************************/
/*! exports provided: ListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListComponent", function() { return ListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var angularfire2_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angularfire2/storage */ "./node_modules/angularfire2/storage/index.js");
/* harmony import */ var angularfire2_storage__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(angularfire2_storage__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _page_login_login_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../page-login/login.service */ "./src/app/page-login/login.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");








var ListComponent = /** @class */ (function () {
    function ListComponent(db, afs, loginService, router, afStorage) {
        var _this = this;
        this.db = db;
        this.afs = afs;
        this.loginService = loginService;
        this.router = router;
        this.afStorage = afStorage;
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            id: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            link: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            descricao: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            categoria: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            empresa: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('clinica'),
            titulo: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            texto: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            url: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('')
        });
        this.loginService.user.subscribe(function (user) {
            if (!user) {
                _this.isLoggin = false;
                _this.router.navigate(['/login']);
            }
            else {
                _this.isLoggin = true;
                _this.router.navigate(['/formvideos']);
            }
        });
    }
    ListComponent.prototype.ngOnInit = function () {
        this.empresaOption = 'clinica';
        this.listarTodosVideos();
        this.listarTodasCategorias();
        this.listarTodasImagens();
    };
    ListComponent.prototype.upload = function (event) {
        this.eventGlobal = event;
    };
    ListComponent.prototype.upload_firebase = function () {
        ////const id = Math.random().toString(36).substring(2);
        this.ref = this.afStorage.ref(this.form.controls['url'].value);
        //console.log("Evento:" + event.target.files[0]);
        this.task = this.ref.put(this.eventGlobal.target.files[0]);
        this.uploadState = this.task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (s) { return s.state; }));
        this.uploadProgress = this.task.percentageChanges();
    };
    ListComponent.prototype.listarTodosVideos = function () {
        this.listvideosCol = this.db.collection('tutorial');
        this.listvideos = this.listvideosCol.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (actions) {
            return actions.map(function (a) {
                var data = a.payload.doc.data();
                var id = a.payload.doc.id;
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({ id: id }, data);
            });
        }));
        return this.listvideos;
    };
    ListComponent.prototype.listarTodasImagens = function () {
        this.listimagensCol = this.db.collection('imagens');
        this.listimagens = this.listimagensCol.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (actions) {
            return actions.map(function (a) {
                var data = a.payload.doc.data();
                var id = a.payload.doc.id;
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({ id: id }, data);
            });
        }));
        return this.listimagens;
    };
    ListComponent.prototype.listarTodasCategorias = function () {
        this.listcategoriaCol = this.db.collection('categorias', function (ref) { return ref.orderBy('nome', 'asc'); });
        this.listcategoria = this.listcategoriaCol.valueChanges();
        return this.listcategoria;
    };
    ListComponent.prototype.insertVideos = function () {
        return this.db.collection('tutorial').add({
            descricao: this.form.controls['descricao'].value,
            link: this.form.controls['link'].value,
            empresa: this.form.controls['empresa'].value,
            categoria: this.form.controls['categoria'].value,
        });
    };
    ListComponent.prototype.updateVideos = function () {
        this.db.doc('tutorial/' + this.form.controls['id'].value).update({
            descricao: this.form.controls['descricao'].value,
            link: this.form.controls['link'].value,
            empresa: this.form.controls['empresa'].value,
            categoria: this.form.controls['categoria'].value,
        });
    };
    ListComponent.prototype.preencherForm = function (id, descricao, link, empresa, categoria) {
        this.form.controls['id'].setValue(id);
        this.form.controls['descricao'].setValue(descricao);
        this.form.controls['link'].setValue(link);
        this.form.controls['empresa'].setValue(empresa);
        this.form.controls['categoria'].setValue(categoria);
        console.log(categoria);
    };
    ListComponent.prototype.preencherFormImagem = function (id, descricao, titulo, texto, url) {
        this.form.controls['id'].setValue(id);
        this.form.controls['descricao'].setValue(descricao);
        this.form.controls['titulo'].setValue(titulo);
        this.form.controls['texto'].setValue(texto);
        this.form.controls['url'].setValue(url);
        console.log("preencherFormImagem");
    };
    ListComponent.prototype.deleteVideo = function (id) {
        if (confirm('Deseja excluir videio ?')) {
            this.db.collection('tutorial').doc(id).delete();
        }
    };
    ListComponent.prototype.limparForm = function () {
        this.form.controls['id'].setValue('');
        this.form.controls['descricao'].setValue('');
        this.form.controls['link'].setValue('');
        this.form.controls['empresa'].setValue('clinica');
        this.form.controls['categoria'].setValue('Cadastro');
    };
    ListComponent.prototype.alterarCategoria = function (tarefa) {
        if (tarefa == '') {
            this.listvideosCol = this.db.collection('tutorial');
        }
        else {
            this.listvideosCol = this.db.collection('tutorial', function (ref) { return ref.where('categoria', '==', tarefa); });
        }
        this.listvideos = this.listvideosCol.valueChanges();
    };
    Object.defineProperty(ListComponent.prototype, "f", {
        get: function () { return this.form.controls; },
        enumerable: true,
        configurable: true
    });
    ListComponent.ctorParameters = function () { return [
        { type: angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] },
        { type: angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] },
        { type: _page_login_login_service__WEBPACK_IMPORTED_MODULE_6__["LoginService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
        { type: angularfire2_storage__WEBPACK_IMPORTED_MODULE_3__["AngularFireStorage"] }
    ]; };
    ListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list',
            template: __webpack_require__(/*! raw-loader!./list.component.html */ "./node_modules/raw-loader/index.js!./src/app/page-formvideos/list/list.component.html"),
            styles: [__webpack_require__(/*! ./list.component.css */ "./src/app/page-formvideos/list/list.component.css")]
        })
    ], ListComponent);
    return ListComponent;
}());



/***/ }),

/***/ "./src/app/page-home/contato/component-contato.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/page-home/contato/component-contato.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".map-container-5 {\r\n    overflow:hidden;\r\n    padding-bottom:56.25%;\r\n    position:relative;\r\n    height:0;\r\n}\r\n.map-container-5 iframe {\r\n    left:0;\r\n    top:0;\r\n    height:100%;\r\n    width:100%;\r\n    position:absolute;\r\n}\r\n.margin-left-custom {\r\n        margin-left: 20px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZS1ob21lL2NvbnRhdG8vY29tcG9uZW50LWNvbnRhdG8uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGVBQWU7SUFDZixxQkFBcUI7SUFDckIsaUJBQWlCO0lBQ2pCLFFBQVE7QUFDWjtBQUNBO0lBQ0ksTUFBTTtJQUNOLEtBQUs7SUFDTCxXQUFXO0lBQ1gsVUFBVTtJQUNWLGlCQUFpQjtBQUNyQjtBQUVBO1FBQ1EsaUJBQWlCO0FBQ3pCIiwiZmlsZSI6InNyYy9hcHAvcGFnZS1ob21lL2NvbnRhdG8vY29tcG9uZW50LWNvbnRhdG8uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXAtY29udGFpbmVyLTUge1xyXG4gICAgb3ZlcmZsb3c6aGlkZGVuO1xyXG4gICAgcGFkZGluZy1ib3R0b206NTYuMjUlO1xyXG4gICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICBoZWlnaHQ6MDtcclxufVxyXG4ubWFwLWNvbnRhaW5lci01IGlmcmFtZSB7XHJcbiAgICBsZWZ0OjA7XHJcbiAgICB0b3A6MDtcclxuICAgIGhlaWdodDoxMDAlO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIHBvc2l0aW9uOmFic29sdXRlO1xyXG59XHJcblxyXG4ubWFyZ2luLWxlZnQtY3VzdG9tIHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMjBweDtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/page-home/contato/component-contato.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/page-home/contato/component-contato.component.ts ***!
  \******************************************************************/
/*! exports provided: ComponentContatoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentContatoComponent", function() { return ComponentContatoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ComponentContatoComponent = /** @class */ (function () {
    function ComponentContatoComponent() {
    }
    ComponentContatoComponent.prototype.ngOnInit = function () {
    };
    ComponentContatoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-component-contato',
            template: __webpack_require__(/*! raw-loader!./component-contato.component.html */ "./node_modules/raw-loader/index.js!./src/app/page-home/contato/component-contato.component.html"),
            styles: [__webpack_require__(/*! ./component-contato.component.css */ "./src/app/page-home/contato/component-contato.component.css")]
        })
    ], ComponentContatoComponent);
    return ComponentContatoComponent;
}());



/***/ }),

/***/ "./src/app/page-home/home/home.component.css":
/*!***************************************************!*\
  !*** ./src/app/page-home/home/home.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\r\n    box-sizing: border-box;\r\n  }\r\n  \r\n  /* Create two equal columns that floats next to each other */\r\n  \r\n  .columnUaiTwo {\r\n    float: left;\r\n    width: 50%;\r\n    padding: 10px;\r\n    height: 300px; /* Should be removed. Only for demonstration */\r\n  }\r\n  \r\n  /* Clear floats after the columns */\r\n  \r\n  .rowUai:after {\r\n    content: \"\";\r\n    display: table;\r\n    clear: both;\r\n  }\r\n  \r\n  p.ex1 {\r\n    max-height: 50px;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZS1ob21lL2hvbWUvaG9tZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksc0JBQXNCO0VBQ3hCOztFQUVBLDREQUE0RDs7RUFDNUQ7SUFDRSxXQUFXO0lBQ1gsVUFBVTtJQUNWLGFBQWE7SUFDYixhQUFhLEVBQUUsOENBQThDO0VBQy9EOztFQUVBLG1DQUFtQzs7RUFDbkM7SUFDRSxXQUFXO0lBQ1gsY0FBYztJQUNkLFdBQVc7RUFDYjs7RUFFQTtJQUNFLGdCQUFnQjtFQUNsQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2UtaG9tZS9ob21lL2hvbWUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIioge1xyXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICB9XHJcbiAgXHJcbiAgLyogQ3JlYXRlIHR3byBlcXVhbCBjb2x1bW5zIHRoYXQgZmxvYXRzIG5leHQgdG8gZWFjaCBvdGhlciAqL1xyXG4gIC5jb2x1bW5VYWlUd28ge1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICB3aWR0aDogNTAlO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGhlaWdodDogMzAwcHg7IC8qIFNob3VsZCBiZSByZW1vdmVkLiBPbmx5IGZvciBkZW1vbnN0cmF0aW9uICovXHJcbiAgfVxyXG4gIFxyXG4gIC8qIENsZWFyIGZsb2F0cyBhZnRlciB0aGUgY29sdW1ucyAqL1xyXG4gIC5yb3dVYWk6YWZ0ZXIge1xyXG4gICAgY29udGVudDogXCJcIjtcclxuICAgIGRpc3BsYXk6IHRhYmxlO1xyXG4gICAgY2xlYXI6IGJvdGg7XHJcbiAgfVxyXG5cclxuICBwLmV4MSB7XHJcbiAgICBtYXgtaGVpZ2h0OiA1MHB4O1xyXG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/page-home/home/home.component.ts":
/*!**************************************************!*\
  !*** ./src/app/page-home/home/home.component.ts ***!
  \**************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_service_caracteristicas_home_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../shared/service/caracteristicas-home.service */ "./src/app/shared/service/caracteristicas-home.service.ts");
/* harmony import */ var _shared_model_mock_texto__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/model/mock_texto */ "./src/app/shared/model/mock_texto.ts");




var HomeComponent = /** @class */ (function () {
    function HomeComponent(caracteristicasHomeService) {
        this.caracteristicasHomeService = caracteristicasHomeService;
        this.img_carrossel_1 = "./assets/img/imagens_tiago/home/6.jpg";
        //img_carrossel_1: any = "https://firebasestorage.googleapis.com/v0/b/boaideianetinformatica.appspot.com/o/foto3?alt=media&token=f6675d09-978d-4780-8867-c9e9cb490614";
        this.img_carrossel_2 = "./assets/img/imagens_tiago/home/1111.jpg";
        this.img_carrossel_3 = "./assets/img/imagens_tiago/home/13.jpg";
        this.img_carrossel_p = "../assets/img/imagens_tiago/home/prontocor2.jpg";
        this.img_carrossel_t = "../assets/img/imagens_tiago/home/taiobeiras2.jpg";
        this.img_carrossel_b = "../assets/img/imagens_tiago/home/brasiliademinas2.jpg";
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.faturamento = _shared_model_mock_texto__WEBPACK_IMPORTED_MODULE_3__["TEXTO"][0].faturamento;
        this.caracteristicasHomeService.hello_word();
        this.c = this.caracteristicasHomeService.getCaracteristicas();
    };
    HomeComponent.ctorParameters = function () { return [
        { type: _shared_service_caracteristicas_home_service__WEBPACK_IMPORTED_MODULE_2__["CaracteristicasHomeService"] }
    ]; };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/page-home/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/page-home/home/home.component.css")]
        })
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/page-home/parceiros/component-parceiros.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/page-home/parceiros/component-parceiros.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2UtaG9tZS9wYXJjZWlyb3MvY29tcG9uZW50LXBhcmNlaXJvcy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/page-home/parceiros/component-parceiros.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/page-home/parceiros/component-parceiros.component.ts ***!
  \**********************************************************************/
/*! exports provided: ComponentParceirosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentParceirosComponent", function() { return ComponentParceirosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ComponentParceirosComponent = /** @class */ (function () {
    function ComponentParceirosComponent() {
        this.img_carrossel_1 = "./assets/img/imagens_tiago/home/6.jpg";
        //img_carrossel_1: any = "https://firebasestorage.googleapis.com/v0/b/boaideianetinformatica.appspot.com/o/foto3?alt=media&token=f6675d09-978d-4780-8867-c9e9cb490614";
        this.img_carrossel_2 = "./assets/img/imagens_tiago/home/1111.jpg";
        this.img_carrossel_3 = "./assets/img/imagens_tiago/home/13.jpg";
        this.img_carrossel_p = "../assets/img/imagens_tiago/home/prontocor2.jpg";
        this.img_carrossel_t = "../assets/img/imagens_tiago/home/taiobeiras2.jpg";
        this.img_carrossel_b = "../assets/img/imagens_tiago/home/brasiliademinas2.jpg";
    }
    ComponentParceirosComponent.prototype.ngOnInit = function () {
    };
    ComponentParceirosComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-component-parceiros',
            template: __webpack_require__(/*! raw-loader!./component-parceiros.component.html */ "./node_modules/raw-loader/index.js!./src/app/page-home/parceiros/component-parceiros.component.html"),
            styles: [__webpack_require__(/*! ./component-parceiros.component.css */ "./src/app/page-home/parceiros/component-parceiros.component.css")]
        })
    ], ComponentParceirosComponent);
    return ComponentParceirosComponent;
}());



/***/ }),

/***/ "./src/app/page-login/login.service.ts":
/*!*********************************************!*\
  !*** ./src/app/page-login/login.service.ts ***!
  \*********************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var rxjs_add_operator_catch__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/operator/catch */ "./node_modules/rxjs-compat/_esm5/add/operator/catch.js");
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angularfire2/auth */ "./node_modules/angularfire2/auth/index.js");
/* harmony import */ var angularfire2_auth__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(angularfire2_auth__WEBPACK_IMPORTED_MODULE_5__);






var LoginService = /** @class */ (function () {
    function LoginService(router, afAuth) {
        this.router = router;
        this.afAuth = afAuth;
        this.user = afAuth.authState;
    }
    LoginService.prototype.login = function (mail, password) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afAuth.auth.signInWithEmailAndPassword(mail, password).then(function (user) {
                localStorage['token'] = user.user.uid;
                if (mail == 'boaideia@boaideianet.com.br') {
                    _this.router.navigate(['/formvideos']);
                }
                else {
                    _this.router.navigate(['/tutoriais']);
                }
            })
                .catch(function (error) {
                alert('Usuario ou Senha incorreta');
                _this.router.navigate(['/login']);
            });
        })
            .catch(function (error) {
            alert('Usuario ou Senha incorreta');
            _this.router.navigate(['/login']);
        });
    };
    LoginService.prototype.logout = function () {
        return this.afAuth.auth.signOut();
    };
    LoginService.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: angularfire2_auth__WEBPACK_IMPORTED_MODULE_5__["AngularFireAuth"] }
    ]; };
    LoginService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        })
    ], LoginService);
    return LoginService;
}());



/***/ }),

/***/ "./src/app/page-login/login/login.component.css":
/*!******************************************************!*\
  !*** ./src/app/page-login/login/login.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2UtbG9naW4vbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/page-login/login/login.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/page-login/login/login.component.ts ***!
  \*****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../login.service */ "./src/app/page-login/login.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var LoginComponent = /** @class */ (function () {
    function LoginComponent(router, loginService) {
        this.router = router;
        this.loginService = loginService;
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.logOn = function () {
        this.loginService.login(this.usuario, this.password);
    };
    LoginComponent.prototype.logout = function () {
        this.loginService.logout();
    };
    LoginComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"] }
    ]; };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/index.js!./src/app/page-login/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/page-login/login/login.component.css")]
        })
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/pages/tutoriais/tutoriais.component.css":
/*!*********************************************************!*\
  !*** ./src/app/pages/tutoriais/tutoriais.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3R1dG9yaWFpcy90dXRvcmlhaXMuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/tutoriais/tutoriais.component.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/tutoriais/tutoriais.component.ts ***!
  \********************************************************/
/*! exports provided: TutoriaisComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TutoriaisComponent", function() { return TutoriaisComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _tutorial_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tutorial.service */ "./src/app/pages/tutoriais/tutorial.service.ts");
/* harmony import */ var _page_login_login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../page-login/login.service */ "./src/app/page-login/login.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var TutoriaisComponent = /** @class */ (function () {
    function TutoriaisComponent(sanitizer, TutorialService, loginService, router) {
        var _this = this;
        this.sanitizer = sanitizer;
        this.TutorialService = TutorialService;
        this.loginService = loginService;
        this.router = router;
        this.url = "https://www.mmlpqtpkasjdashdjahd.com";
        this.loginService.user.subscribe(function (user) {
            if (!user) {
                _this.isLoggin = false;
                _this.router.navigate(['/login']);
            }
            else {
                _this.isLoggin = true;
                _this.router.navigate(['/tutoriais']);
            }
        });
    }
    TutoriaisComponent.prototype.ngOnInit = function () {
        this.categoriaModelo = 'Todos';
        // INICIANDO COM AS CATEGORIAS COMERCIAIS
        this.categoriaSelecionada('Comercial');
        // INICIANDO COM OS VIDEOS SEM FILTRO DE CATEGORIA
        this.listavideosClinica('');
        // INICIANDO COM OS VIDEOS SEM FILTRO DE CATEGORIA
        this.listavideosComercial('');
    };
    // LISTA CATEGORIA PELA EMPRESA QUE ESTA SELECIONANDA
    TutoriaisComponent.prototype.categoriaSelecionada = function (cat) {
        this.categoria = this.TutorialService.getCategoriasSelecionada(cat);
    };
    // LISTA VIDEOS DA EMPRESA/CATEGORIA SELECIONADA
    TutoriaisComponent.prototype.listavideosComercial = function (cat) {
        this.empresaModelo = 'Comercial';
        this.posts = this.TutorialService.getVideosComercial(cat);
    };
    // LISTA VIDEOS DA EMPRESA/CATEGORIA SELECIONADA
    TutoriaisComponent.prototype.listavideosClinica = function (cat) {
        this.empresaModelo = 'Clínica';
        this.posts1 = this.TutorialService.getVideosClinica(cat);
    };
    // MUDA CATEGORIA NO CLICK
    TutoriaisComponent.prototype.filtroCategoriaSelecao = function (cat, empresa) {
        this.categoriaModelo = cat;
        if (empresa == 'Clínica' || empresa == 'Comercial') {
            this.empresaModelo = empresa;
        }
        console.log("todos " + cat);
        if (this.empresaModelo == 'Clínica') {
            if (cat == 'Todos') {
                cat = '';
                this.posts1 = this.TutorialService.getVideosClinica(cat);
            }
            else {
                this.posts1 = this.TutorialService.getVideosClinica(cat);
            }
        }
        if (this.empresaModelo == 'Comercial') {
            if (cat == 'Todos') {
                cat = '';
                this.posts = this.TutorialService.getVideosComercial(cat);
            }
            else {
                this.posts = this.TutorialService.getVideosComercial(cat);
            }
        }
        if (cat == '') {
            this.categoriaModelo = 'Todos';
        }
    };
    TutoriaisComponent.prototype.photoURL = function () {
        return this.sanitizer.bypassSecurityTrustResourceUrl("https://www.mmlpqtpkasjdashdjahd.com");
    };
    TutoriaisComponent.ctorParameters = function () { return [
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"] },
        { type: _tutorial_service__WEBPACK_IMPORTED_MODULE_3__["TutorialService"] },
        { type: _page_login_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
    ]; };
    TutoriaisComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tutoriais',
            template: __webpack_require__(/*! raw-loader!./tutoriais.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/tutoriais/tutoriais.component.html"),
            styles: [__webpack_require__(/*! ./tutoriais.component.css */ "./src/app/pages/tutoriais/tutoriais.component.css")]
        })
    ], TutoriaisComponent);
    return TutoriaisComponent;
}());



/***/ }),

/***/ "./src/app/pages/tutoriais/tutorial.directive.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/tutoriais/tutorial.directive.ts ***!
  \*******************************************************/
/*! exports provided: TutorialDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TutorialDirective", function() { return TutorialDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _tutorial_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tutorial.service */ "./src/app/pages/tutoriais/tutorial.service.ts");
/* harmony import */ var _tutoriais_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tutoriais.component */ "./src/app/pages/tutoriais/tutoriais.component.ts");





var TutorialDirective = /** @class */ (function () {
    function TutorialDirective(calculadoraService, TutoriaisComponent) {
        this.calculadoraService = calculadoraService;
        this.TutoriaisComponent = TutoriaisComponent;
    }
    TutorialDirective.prototype.ngOnInit = function () {
        this.postsTest = null;
        this.categoria = null;
        this.TutoriaisComponent.posts = null;
        this.myfuction(this.cat);
        console.log("teste2 " + this.TutoriaisComponent.posts);
    };
    TutorialDirective.prototype.myfuction = function (categoria) {
        //	this.TutoriaisComponent.posts = this.calculadoraService.getVideosClinica();
        console.log(this.cat);
        console.log(this.TutoriaisComponent.posts);
    };
    TutorialDirective.ctorParameters = function () { return [
        { type: _tutorial_service__WEBPACK_IMPORTED_MODULE_2__["TutorialService"] },
        { type: _tutoriais_component__WEBPACK_IMPORTED_MODULE_3__["TutoriaisComponent"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('categoria')
    ], TutorialDirective.prototype, "cat", void 0);
    TutorialDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[fundoAmarelo]'
        })
    ], TutorialDirective);
    return TutorialDirective;
}());



/***/ }),

/***/ "./src/app/pages/tutoriais/tutorial.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/tutoriais/tutorial.service.ts ***!
  \*****************************************************/
/*! exports provided: TutorialService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TutorialService", function() { return TutorialService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__);



var TutorialService = /** @class */ (function () {
    function TutorialService(db) {
        this.db = db;
    }
    TutorialService.prototype.getCategoriasSelecionada = function (cat) {
        this.categoriaColClinica = this.db.collection('categorias');
        this.categoriaClinica = this.categoriaColClinica.valueChanges();
        return this.categoriaClinica;
    };
    TutorialService.prototype.getVideosComercial = function (cat) {
        if (cat == '') {
            this.postsCol = this.db.collection('tutorial', function (ref) { return ref.where('empresa', '==', 'Comercial'); });
        }
        else {
            this.postsCol = this.db.collection('tutorial', function (ref) { return ref.where('empresa', '==', 'Comercial').where('categoria', '==', cat); });
        }
        this.posts = this.postsCol.valueChanges();
        return this.posts;
    };
    TutorialService.prototype.getVideosClinica = function (cat) {
        if (cat == '') {
            this.postsCol1 = this.db.collection('tutorial', function (ref) { return ref.where('empresa', '==', 'Clínica'); });
        }
        else {
            this.postsCol1 = this.db.collection('tutorial', function (ref) { return ref.where('empresa', '==', 'Clínica').where('categoria', '==', cat); });
        }
        this.posts1 = this.postsCol1.valueChanges();
        return this.posts1;
    };
    TutorialService.ctorParameters = function () { return [
        { type: angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] }
    ]; };
    TutorialService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        })
    ], TutorialService);
    return TutorialService;
}());



/***/ }),

/***/ "./src/app/shared/model/mock_caracteristica.ts":
/*!*****************************************************!*\
  !*** ./src/app/shared/model/mock_caracteristica.ts ***!
  \*****************************************************/
/*! exports provided: CARACTERISTICAS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CARACTERISTICAS", function() { return CARACTERISTICAS; });
var CARACTERISTICAS = [
    { icon_material: 'autorenew', titulo: '1111111', texto: 'AA111111111' },
    { icon_material: 'backup', titulo: '2222222', texto: 'AA222222222' },
    { icon_material: 'autorenew', titulo: '3333333', texto: 'AA333333333' },
    { icon_material: 'autorenew', titulo: '4444444', texto: 'AA444444444' },
    { icon_material: 'autorenew', titulo: '5555555', texto: 'AA555555555' },
    { icon_material: 'autorenew', titulo: '6666666', texto: 'AA666666666' },
    { icon_material: 'autorenew', titulo: '7777777', texto: 'AA777777777' },
    { icon_material: 'autorenew', titulo: '8888888', texto: 'AA888888888' },
    { icon_material: 'autorenew', titulo: '9999999', texto: 'AA999999999' }
];


/***/ }),

/***/ "./src/app/shared/model/mock_texto.ts":
/*!********************************************!*\
  !*** ./src/app/shared/model/mock_texto.ts ***!
  \********************************************/
/*! exports provided: TEXTO */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TEXTO", function() { return TEXTO; });
var TEXTO = [
    { faturamento: 'Convênios e Particulares\nArquivo Eletrônico TISS' },
];


/***/ }),

/***/ "./src/app/shared/service/caracteristicas-home.service.ts":
/*!****************************************************************!*\
  !*** ./src/app/shared/service/caracteristicas-home.service.ts ***!
  \****************************************************************/
/*! exports provided: CaracteristicasHomeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CaracteristicasHomeService", function() { return CaracteristicasHomeService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _model_mock_caracteristica__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/mock_caracteristica */ "./src/app/shared/model/mock_caracteristica.ts");



var CaracteristicasHomeService = /** @class */ (function () {
    function CaracteristicasHomeService() {
    }
    CaracteristicasHomeService.prototype.hello_word = function () {
        console.log("Hello Word");
    };
    CaracteristicasHomeService.prototype.getCaracteristicas = function () {
        return _model_mock_caracteristica__WEBPACK_IMPORTED_MODULE_2__["CARACTERISTICAS"];
    };
    CaracteristicasHomeService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        })
    ], CaracteristicasHomeService);
    return CaracteristicasHomeService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    firebase: {
        apiKey: "AIzaSyC8FnX633g601VFCiz2tVwpdqYfbE__FcE",
        authDomain: "boaideianetinformatica.firebaseapp.com",
        databaseURL: "https://boaideianetinformatica.firebaseio.com",
        projectId: "boaideianetinformatica",
        storageBucket: "boaideianetinformatica.appspot.com",
        messagingSenderId: "980360100677",
        appId: "1:980360100677:web:a642d6e20da6ab2f"
    }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_4__);





if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Public\GERSON\Angular\BOA_IDEIA\atual_site_boa_ideia\site_boa_ideia\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es5.js.map